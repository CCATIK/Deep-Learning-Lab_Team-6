The Deep Learning Lab is aiming to impart students knowledge in the fields of machine learning and pattern recognition by practical application of corresponding methods. Students learn to implement and configure classification algorithms, e.g., Linear discriminative functions, Support-Vector-Machines, and neural networks. Modern concepts and approaches, especially deep learning are also part of the computational experiments. To motivate subsequent self-study only free-to-use datasets as well as Open-Source-Software will be used. For the computational complex training algorithms students are provided access to powerful centralized GPU (Graphical Processing Unit) hardware.

The Deep Learning Lab is divided in three parts of practice:

• First the students get an introduction to the Python programming language and all used libraries.

• Second will be practical excersises with the aforementioned methods.

• Third - in the so called Deep Learning Challenge - students use the learned methods to develop an own machine learning system and compete among each other to reach the best result on a predefined dataset. Therefore, the students will be provided with real data from the industrial field of application. 

To boost the ability to work in a team the excercises and the Deep Learning Challenge will be conducted in groups of 2-3 students. 

The results of the first and the second parts of practice will be reviewed in a colloquium with the supervising assistant. The systems from the Deep Learning Challenge will be presented in short talks with representatives of the data-providing industry partners in a closing event.