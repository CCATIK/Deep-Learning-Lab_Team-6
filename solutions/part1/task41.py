
def num_and_squared(txt):
    num = int(txt)

    if (num <= 0) or (int(num) != num):
        raise(Exception('Input has to be an integer greater than or equal to 0'))

    dst = dict()

    for i in range(num):
        dst[i] = i*i

    return dst

while True:
    try:
        val = input()
        res = num_and_squared(val)

        print(res)
        break

    except Exception as e:
        print(e)
