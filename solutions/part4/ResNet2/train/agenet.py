import torch
import torchvision.models as models
import torch.nn as nn


class AgeResNet(models.resnet.ResNet):
    def __init__(self, imagenet_base=None, agenet_base=None, softmax=False):
        block = models.resnet.Bottleneck

        super(AgeResNet, self).__init__(block, [3, 4, 6, 3])

        if imagenet_base:
            self.load_state_dict(torch.load(imagenet_base))
            for _, param in self.named_children():
                param.requires_grad = False

        # The original ResNet has 1000 output classes,
        # we need only 100 and 2
        self.fc1 = nn.Linear(512 * block.expansion, 100)
        self.fc2 = nn.Linear(512 * block.expansion, 2)
        if softmax:
            self.softmax = nn.Softmax(1)
        else:
            self.softmax = None

        if agenet_base:
            self.load_state_dict(torch.load(agenet_base))

    def save(self, path):
        torch.save(self.state_dict(), path)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x1 = self.fc1(x)
        x2 = self.fc2(x)
        if self.softmax:
            x1 = self.softmax(x1)
            x2 = self.softmax(x2)

        return x1, x2
