from train.dataset import SplitDataset
from torch.utils.data import DataLoader
import os
import time
import torch.nn as nn
import torch


def in_range(xx, yy):
    return 1 if xx - 2.5 < yy < xx + 2.5 else 0


class Trainer(object):
    def __init__(self, dataset, batch_size, model, device, optimizer, loss_function, report, generator, scheduler=None):
        set_train, val_test_set = SplitDataset.split(dataset, 0.995)
        set_val, set_test = SplitDataset.split(val_test_set, 0.80)

        self.loader_train = DataLoader(set_train, batch_size=batch_size, shuffle=True)
        self.loader_val = DataLoader(set_val, batch_size=batch_size)
        self.loader_test = DataLoader(set_test, batch_size=batch_size)

        self.model = model
        self.optimizer = optimizer
        self.loss = loss_function
        self.device = device
        self.report = report
        self.generator = generator
        self.softmax = nn.Softmax(1)
        self.scheduler = scheduler

    def load_test_images(self, report):
        images, target_age, target_gender = next(iter(self.loader_test))
        report.add_test_images(images, target_age, target_gender)

    def test(self):
        if (len(self.loader_test.dataset)) > 0:
            self.model.eval()
            images, target_age, target_gender = next(iter(self.loader_test))
            images = images.to(self.device)

            output_age, output_gender = self.model.forward(images)
            output_age = self.softmax(output_age)
            output_gender = self.softmax(output_gender)
            output_age = output_age.to('cpu').detach()
            output_gender = output_gender.to('cpu').detach()
            self.report.add_test_image_prediction((output_age.numpy(), output_gender.numpy()))

    def validate(self, epoch):
        if (len(self.loader_val.dataset)) > 0:
            with torch.no_grad():
                self.model.eval()
                loss = 0
                correct_age = 0
                correct_gender = 0
                counter = 0
                for (batch_idx, (image, target_age, target_gender)) in enumerate(self.loader_val):
                    counter += 1
                    image = image.to(self.device)
                    target_age = target_age.to(self.device)
                    target_gender = target_gender.to(self.device)

                    output_age, output_gender = self.model.forward(image)     #output_age + output_gender on gpu
                    loss += self.loss(output_age, output_gender, target_age, target_gender)
                    output_age = output_age.to('cpu').detach()                         #output_age to cpu + detach
                    output_gender = output_gender.to('cpu').detach()                   #output_gender to cpu + detach
                    target_gender = target_gender.to('cpu').detach()
                    target_age = target_age.to('cpu').detach()
                    pred_age = output_age.max(1, keepdim=True)[1]
                    pred_gender = output_gender.max(1, keepdim=True)[1]
                    target_gen = target_gender.max(1, keepdim=True)[1]
                    correct_age += pred_age.map_(target_age.view_as(pred_age), in_range).sum().item()
                    correct_gender += pred_gender.eq(target_gen.view_as(pred_gender)).sum().item()
                    del image

                loss /= counter
                if self.scheduler:
                    self.scheduler.step(loss)
                val_acc_gender = 100. * correct_gender / len(self.loader_val.dataset)
                val_acc_age = 100. * correct_age / len(self.loader_val.dataset)

                self.report.add_accuracy_validation_age(0, epoch, val_acc_age)
                self.report.add_accuracy_validation_gender(0, epoch, val_acc_gender)
                self.report.add_loss_validation(0, epoch, loss.to('cpu').detach())
                if self.report.min_val_flag:
                    self.model.save(
                        os.path.join(self.report.get_path(), 'best_val_loss.pth'))
                    self.report.min_val_flag = False

    def add_report_data(self, ep, batch_idx, train_acc_age, train_acc_gender, loss):
        self.report.add_loss_training(batch_idx, ep, loss)
        self.report.add_accuracy_training_age(batch_idx, ep, train_acc_age)
        self.report.add_accuracy_training_gender(batch_idx, ep, train_acc_gender)

    def train(self, epoch, log_interval=100):
        self.model.train()
        iteration = 0
        batch_size = self.loader_train.batch_size
        self.load_test_images(self.report)
        # Run through training set and update network
        t = time.time()
        for ep in range(epoch):
            correct_age = 0
            correct_gender = 0
            self.validate(ep)
            self.test()

            for (batch_idx, (images, target_age, target_gender)) in enumerate(self.loader_train):

                images = images.to(self.device)
                target_age = target_age.to(self.device)
                target_gender = target_gender.to(self.device)

                self.optimizer.zero_grad()
                output_age, output_gender = self.model.forward(images)

                loss = self.loss(output_age, output_gender, target_age, target_gender)

                loss.backward()
                self.optimizer.step()

                output_age = output_age.to('cpu').detach()
                output_gender = output_gender.detach()
                target_gender = target_gender.detach()
                target_age = target_age.to('cpu').detach()

                pred_age = output_age.max(1, keepdim=True)[1]
                pred_gender = output_gender.max(1, keepdim=True)[1]
                target_gen = target_gender.max(1, keepdim=True)[1]
                correct_age += pred_age.map_(target_age.view_as(pred_age), in_range).sum().item()
                correct_gender += pred_gender.eq(target_gen.view_as(pred_gender)).sum().item()

                if iteration % log_interval == 0:
                    train_acc_age = 100.*correct_age/((batch_idx+1)*batch_size)
                    train_acc_gender = 100.*correct_gender/((batch_idx+1)*batch_size)
                    self.add_report_data(ep, batch_idx, train_acc_age, train_acc_gender, loss.to('cpu').detach().item())
                    #self.add_report_data(ep, batch_idx, 0, 0, loss.to('cpu').detach().item())
                    if time.time() - t > 300:
                        t = time.time()
                        self.generator.generate_html()

                iteration += 1
                del images
                del target_age
                del target_gender
                del loss

            if self.report.min_train_flag:
                self.model.save(
                    os.path.join(self.report.get_path(), 'best_train_loss.pth'))
                self.report.min_train_flag = False
            self.generator.generate_html()
            print("e {} Epoch: {}".format(time.strftime('%d.%m.%Y - %H:%M', time.localtime(time.time())), ep))




