import os
import sqlite3

import numpy as np
import torch
from PIL import Image
from torchvision import transforms
from torch.utils.data import Dataset


class ImdbWikiDataset(Dataset):
    def __init__(self, database):
        self.conn = sqlite3.connect(database)

        # # src: http://blog.outcome.io/pytorch-quick-start-classifying-an-image/
        # self.preprocess = transforms.Compose([
        #     transforms.Resize(224),
        #     transforms.CenterCrop(224),
        #     transforms.ToTensor(),
        #     transforms.Normalize(
        #         mean=[0.485, 0.456, 0.406],
        #         std=[0.229, 0.224, 0.225]
        #     )
        # ])

        self.preprocess = transforms.Compose([
            transforms.RandomRotation(15),
            transforms.Resize(250),
            transforms.RandomCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ColorJitter(
                brightness=0.1,
                contrast=0.1,
                saturation=0.1,
                hue=0.05,
            ),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            ),
        ])

        self.weights = np.arange(-99, 100) ** 2

    def __len__(self):
        c = self.conn.cursor()

        c.execute('SELECT COUNT(*) FROM photos')

        return c.fetchone()[0]

    def __getitem__(self, idx):
        c = self.conn.cursor()

        c.execute('SELECT dob, taken, gender, path FROM photos WHERE id=?', (idx + 1,))

        (dob, taken, gender, path) = c.fetchone()

        age = int((taken - dob) / (365 * 24 * 60 * 60) + 0.5)
        age = min(max(age, 0), 99)

        img_pil = Image.open(path).convert('RGB')
        img_tensor = self.preprocess(img_pil)

        if gender == 'M':
            label_gender = torch.Tensor([0, 1])
        elif gender == 'F':
            label_gender = torch.Tensor([1, 0])
        else:
            raise Exception('Unkown gender: ' + gender)

        return img_tensor, age, label_gender

    def get_weights(self):
        c = self.conn.cursor()
        c.execute('SELECT dob, taken FROM photos')
        output = c.fetchall()
        array = np.array(output)
        array = array.reshape(array.shape[1], array.shape[0])

        age = (array[1] - array[0]) / (365 * 24 * 60 * 60) + 0.5
        age = age.astype(int)
        age[age < 0] = 0
        age[age > 99] = 99
        count = np.bincount(age)
        weights = count / (count.sum())
        return torch.from_numpy(weights).float()


class SplitDataset(Dataset):
    def __init__(self, base, start, end):
        self.base = base
        self.start = start
        self.end = end

    def __len__(self):
        return self.end - self.start

    def __getitem__(self, idx):
        if idx >= len(self):
            raise IndexError('dataset index out of range')

        return self.base[self.start + idx]

    @classmethod
    def split(cls, dataset, ratio):
        full_len = len(dataset)
        first_len = int(full_len * ratio)

        first = cls(dataset, 0, first_len)
        second = cls(dataset, first_len, full_len)

        return first, second
