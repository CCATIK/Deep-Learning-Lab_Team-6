#!/usr/bin/env python3

# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- (1) IMPORTS --------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

import time
import argparse
import sys

import torch

from train.dataset import ImdbWikiDataset, SplitDataset
from numpy.random import uniform
from train.agenet import AgeResNet
from train.trainer import Trainer
from utils.report import Report
from utils.report import ReportGenerator
from torch import optim
import torch.nn as nn


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- LOSS FUNCTIONS ------------------------------------------------------ #
# -------------------------------------------------------------------------------------------------------------------- #

def loss(output_age, output_gender, labels_age, labels_gender, weights):
    # Calculate loss with provided loss function
    loss_cross_entropy = nn.CrossEntropyLoss(weights)
    loss_mse = nn.MSELoss()
    loss_age = loss_cross_entropy(output_age, labels_age)
    loss_gender = loss_mse(output_gender, labels_gender)

    return loss_age + loss_gender


loss.__name__ = 'Age -> Cross Entropy Loss, Gender -> MSE Loss'

# def cross_entropy(output_age, output_gender, target_age, target_gender):
#     def normal(x):
#         return 1 / math.sqrt(2 * math.pi * 2.5 ** 2) * math.e ** (-(x) ** 2 / (2 * 2.5 ** 2))
#
#     for i in range(0, 200):
#         values.append(normal(-100 + i))
#
#     distr = np.array(values)
#     age_label = array[100 - target_age:200 - target_age]
#
#     logsoftmax = nn.LogSoftmax()
#
#     return torch.mean(torch.sum(- output_age * logsoftmax(pred), 1))


if __name__ == '__main__':
    import sys
    print(sys.argv)

    parser = argparse.ArgumentParser(description='Group 6 AgeResNet trainer')

    parser.add_argument('-b', '--minibatch', help='Minibatch size', required=False, default=256, type=int)
    parser.add_argument('-l', '--limit', help='Limit Dataset to n images', required=False, default=0, type=int)
    parser.add_argument('-i', '--imagenet', help='Imagenet base network', required=False, default='', type=str)
    parser.add_argument('-a', '--ageresnet', help='AgeResNet base network', required=False, default='', type=str)
    parser.add_argument('-d', '--dataset', help='Dataset Database', required=False, default='data/meta.db', type=str)
    parser.add_argument('-u', '--url', help='URL to PUT reports to', required=False, default=None, type=str)
    parser.add_argument('-o', '--outdir', help='Output directory', required=False, default='.', type=str)
    parser.add_argument('-e', '--epoch', help='Number of epochs', required=False, default=0, type=int)
    parser.add_argument('-c', '--count', help='Maximal runs', required=False, default=1, type=int)
    parser.add_argument('-r', '--learning', help='Learning rate', required=False, default=0, type=float)
    parser.add_argument('-g', '--githash', help='Hash of git commit', required=False, default='', type=str)
    parser.add_argument('-v', '--log_interval', help='Log intervall in number of computed batches', required=False, default=100, type=int)
    args = parser.parse_args()

    dataset = ImdbWikiDataset(args.dataset)
    weights = dataset.get_weights()
    if args.limit != 0:
        dataset = SplitDataset(dataset, 0, args.limit)

    if args.count > 1:
        learning_rate = 10 ** uniform(-1, -6, args.count)
    elif args.learning > 0:
        learning_rate = [args.learning]
    else:
        learning_rate = [8.0e-5]

    report_generator = ReportGenerator(args.outdir, args.url, ' '.join(sys.argv), args.githash)

    for lr in learning_rate:
        network = AgeResNet(args.imagenet, args.ageresnet).to(device)
        optimizer = optim.Adam(network.parameters(), lr=lr)
        scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=5)
        report = Report(args.outdir, lr, loss.__name__, optimizer, args.minibatch, len(dataset))
        report_generator.add_report(report)
        trainer = Trainer(dataset, args.minibatch, network, device, optimizer,
                          lambda a, b, c, d: loss(a, b, c, d, weights.to(device)), report, report_generator)
        print('s {} Start training using device: {} Batch size: {} lr: {}'.format(
            time.strftime('%d.%m.%Y - %H:%M', time.localtime(time.time())), device, args.minibatch, lr))
        trainer.train(args.epoch, log_interval=args.log_interval)
