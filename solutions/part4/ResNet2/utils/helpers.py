import io
import base64
import time

import numpy as np

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from jinja2 import Template

from torchvision import transforms
import os


class RelTime(object):
    def __init__(self):
        self.start = time.time()

    def delta(self):
        return time.time() - self.start


img_process = transforms.Compose([
    transforms.Normalize(
        mean=[-0.485 / 0.229, -0.456 / 0.224, -0.406 / 0.225],
        std=[1 / 0.229, 1 / 0.224, 1 / 0.225]
    ),
    transforms.ToPILImage(),
])

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))


def plot_vs_epochs(label, *values):
    x = np.arange(len(values[0])) + 1

    y_s = list(np.array(value) for value in values)

    fig = Figure()
    FigureCanvas(fig)

    dims = [x, None] * len(y_s)
    dims[1::2] = y_s

    ax = fig.add_subplot(1, 1, 1)
    ax.plot(*dims)
    ax.set_xlabel('epochs')
    ax.set_ylabel(label)

    buff = io.BytesIO()
    fig.savefig(buff, format='svg')
    b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

    return b64


def plot_vs_epochs_sub(label, *values):
    fig = Figure()
    FigureCanvas(fig)
    sets = len(label)

    for i in range(sets):
        x = np.arange(len(values[i][0])) + 1

        y_s = list(np.array(value) for value in values[i])

        dims = [x, None] * len(y_s)
        dims[1::2] = y_s

        ax = fig.add_subplot(sets, 1, i + 1)
        ax.plot(*dims)
        ax.set_ylabel(label[i])

    buff = io.BytesIO()
    fig.savefig(buff, format='svg')
    b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

    return b64


def plot_ages_gender(age, gender):
    x = np.arange(len(age))

    fig = Figure()
    FigureCanvas(fig)

    ax_g = fig.add_subplot(2, 1, 1)
    ax_g.bar([0, 1], gender, 0.35)
    ax_g.set_xticks([0, 1])
    ax_g.set_xticklabels(['Female', 'Male'])

    ax_a = fig.add_subplot(2, 1, 2)
    ax_a.plot(x, age)
    ax_a.set_xlabel('age')
    ax_a.set_ylabel('probability')

    buff = io.BytesIO()
    fig.savefig(buff, format='svg')
    b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

    return b64


def convert_images(test_images, test_guess_a, test_guess_g, age_label, gender_label):
    ret = list()

    for i in range(test_images.shape[0]):
        img = img_process(test_images[i, :, :])

        buff = io.BytesIO()
        img.save(buff, 'png')
        b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

        age = test_guess_a[i].detach().numpy()
        gender = test_guess_g[i].detach().numpy()
        label_a = age_label[i].detach().numpy()
        label_g = 'Female' if gender_label[i][0] else 'Male'
        precited_age = age_predict(age)

        guess = plot_ages_gender(age, gender)

        ret.append((b64, guess, "Age = {} Predict = {}".format(label_a, precited_age), label_g))

    return ret


def age_predict(guess):
    age = np.arange(100)
    prediction = age.dot(guess)
    return prediction


def gen_report(fname, githash, train_losses, val_losses, gpu_rams,
               learn_rates, test_images, test_guess_a, test_guess_g, args, epoch_times, start_time, update_time,
               next_time, age, gender):
    with open(os.path.join(__location__, 'templates/report.tpl.html')) as fd:
        template = Template(fd.read())

    b64svg_losses = plot_vs_epochs('loss', train_losses, val_losses)
    b64svg_gpuram = plot_vs_epochs('gpu ram', gpu_rams)
    b64svg_lr = plot_vs_epochs('learn rate', learn_rates)
    b64svg_time = plot_vs_epochs('time', epoch_times)

    testset_images = convert_images(test_images, test_guess_a, test_guess_g, age, gender)
    html = template.render(
        b64svg_losses=b64svg_losses,
        b64svg_gpuram=b64svg_gpuram,
        githash=githash,
        b64svg_lr=b64svg_lr,
        b64svg_time=b64svg_time,
        testset_images=testset_images,
        args=args,
        start_time=start_time,
        update_time=update_time,
        next_time=next_time,
    )

    with open(fname, 'w') as fd:
        fd.write(html)


def gen_report_optim(fname, githash, args, learning_rates, train_losses, val_losses, acc_a,
                     acc_g, start_time, update_time, next_time):
    with open(os.path.join(__location__, 'templates/report_otim.tpl.html')) as fd:
        template = Template(fd.read())

    train_loss = list()
    val_loss = list()
    for i in range(len(learning_rates)):
        temp = train_losses[i][-1]
        train_loss.append(temp)
        temp = val_losses[i][-1]
        val_loss.append(temp)

    train_loss_np = np.array(train_loss)
    train_loss_min = np.argpartition(train_loss_np, 0 if len(train_loss_np) < 5 else 5 - 1)
    train_loss_ind = np.zeros(train_loss_np.shape)
    train_loss_ind[train_loss_min[:5]] = 1

    val_loss_np = np.array(val_loss)
    val_loss_min = np.argpartition(val_loss_np, 0 if len(val_loss_np) < 5 else 5 - 1)
    val_loss_ind = np.zeros(val_loss_np.shape)
    val_loss_ind[val_loss_min[:5]] = 1

    train_loss = ["%8.6f" % loss for loss in train_loss]
    val_loss = ["%8.6f" % loss for loss in val_loss]
    accuracy_g = ["{:4.2f} %".format(acc) for acc in acc_g]
    accuracy_a = ["{:4.2f} %".format(acc) for acc in acc_a]
    html = template.render(
        args=args,
        train_loss=train_loss,
        githash=githash,
        val_loss=val_loss,
        train_min=train_loss_ind,
        val_min=val_loss_ind,
        acc_age=accuracy_a,
        acc_gender=accuracy_g,
        start_time=start_time,
        update_time=update_time,
        next_time=next_time,
        learning_rates=learning_rates
    )

    with open(fname, 'w') as fd:
        fd.write(html)


def put_file(fname, content_type, url):
    try:
        import requests

        with open(fname, 'rb') as fd:
            headers = {'Content-Type': content_type}
            requests.put(url, data=fd, headers=headers)

    except Exception as e:
        print('e PUT failed:', e)
