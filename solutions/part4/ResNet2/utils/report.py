import time
import os
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import io
import base64
import numpy as np
from jinja2 import Template
from torchvision import transforms
import math

img_process = transforms.Compose([
    transforms.Normalize(
        mean=[-0.485 / 0.229, -0.456 / 0.224, -0.406 / 0.225],
        std=[1 / 0.229, 1 / 0.224, 1 / 0.225]
    ),
    transforms.ToPILImage(),
])

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))


class Report(object):

    def __init__(self, path, learning_rate, loss_function, optimizer, batch_size, size):
        self.path = path
        self.learning_rate = learning_rate
        self.loss_function = loss_function
        self.optimizer = optimizer
        self.batch_size = batch_size
        self.dataset_size = size
        self.loss_training = list()
        self.loss_validation = list()
        self.accuracy_training_age = list()
        self.accuracy_training_gender = list()
        self.accuracy_validation_age = list()
        self.accuracy_validation_gender = list()
        self.test_images = list()
        self.test_image_prediction = list()
        self.gpu_ram = list()
        self.min_train_loss = math.inf
        self.min_train_flag = False
        self.min_val_flag = False
        self.min_val_loss = math.inf
        self.factor = self.batch_size/self.dataset_size
        self.min_train_loss = math.inf
        self.min_val_loss = math.inf
        self.epoch_time_estimate = 0
        self.last_ts = time.time()
        self.index = 0.1

    def get_path(self):
        return self.path

    def get_args(self):
        return 'lr_{}_bs_{}_setsize_{}'.format(self.learning_rate, self.batch_size, self.dataset_size)

    def add_loss_training(self, batch_idx, epoch, loss_training):
        if loss_training < self.min_train_loss:
            self.min_train_loss = loss_training
            self.min_train_flag = True
        self.loss_training.append((epoch+(batch_idx*self.factor), loss_training))
        c_ts = time.time()
        index = epoch*self.dataset_size+batch_idx*self.batch_size
        self.epoch_time_estimate = (c_ts-self.last_ts)/(index-self.index)*self.dataset_size
        self.index = index
        self.last_ts = c_ts

    def add_accuracy_training_gender(self, batch_idx, epoch, accuracy_gender):
        self.accuracy_training_gender.append((epoch+(batch_idx*self.factor), accuracy_gender))

    def add_accuracy_training_age(self, batch_idx, epoch, accuracy_age):
        self.accuracy_training_age.append((epoch+(batch_idx*self.factor), accuracy_age))

    def add_loss_validation(self, batch_idx, epoch, loss_validation):
        if loss_validation < self.min_val_loss:
            self.min_val_loss = loss_validation
            self.min_val_flag = True
        self.loss_validation.append((epoch+(batch_idx*self.factor), loss_validation))

    def add_accuracy_validation_age(self, batch_idx, epoch, accuracy_age):
        self.accuracy_validation_age.append((epoch+(batch_idx*self.factor), accuracy_age))

    def add_accuracy_validation_gender(self, batch_idx, epoch, accuracy_gender):
        self.accuracy_validation_gender.append((epoch+(batch_idx*self.factor), accuracy_gender))

    def add_test_images(self, images, label_age, label_gender):
        for i in range(images.shape[0]):
            image = img_process(images[i, :, :])
            self.test_images.append((image, label_age[i], label_gender[i]))

    def add_test_image_prediction(self, predictions):
        self.test_image_prediction.append(predictions)

    def add_gpu_rams(self, gpu_ram):
        self.gpu_ram.append(gpu_ram)

    def generate_plot(self, values, xlabel, ylabel):
        fig = Figure()
        FigureCanvas(fig)
        max_value = 0

        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        for x, y in values:
            if x is None or y is None:
                continue
            ax.plot(x, y)
            max_value = int(x.max())
            #max_value = max_value.astype(int)

        x_vertical = range(0, max_value, 1)
        for x_v in x_vertical:
            ax.axvline(x_v, linestyle=':', linewidth=1, color='k', dashes=(1, 15))
        buff = io.BytesIO()
        fig.savefig(buff, format='svg')
        b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')
        return b64

    def plot_loss(self):
        x_val, y_val, x_train, y_train = None, None, None, None
        if self.loss_validation:
            values_val = np.array(self.loss_validation)
            values_val = values_val.transpose()
            x_val = values_val[0]
            y_val = values_val[1]

        if self.loss_training:
            values_train = np.array(self.loss_training)
            values_train = values_train.transpose()
            x_train = values_train[0]
            y_train = values_train[1]

        b64 = self.generate_plot([(x_train, y_train), (x_val, y_val)], 'epoch', 'loss')
        return b64

    def plot_accuracy_age(self):
        x_val, y_val, x_train, y_train = None, None, None, None
        if self.accuracy_validation_age:
            values_val = np.array(self.accuracy_validation_age)
            values_val = values_val.transpose()
            x_val = values_val[0]
            y_val = values_val[1]

        if self.accuracy_training_age:
            values_train = np.array(self.accuracy_training_age)
            values_train = values_train.transpose()
            x_train = values_train[0]
            y_train = values_train[1]

        b64 = self.generate_plot([(x_train, y_train), (x_val, y_val)], 'epoch', 'accuracy age')
        return b64

    def plot_accuracy_gender(self):
        x_val, y_val, x_train, y_train = None, None, None, None
        if self.accuracy_validation_gender:
            values_val = np.array(self.accuracy_validation_gender)
            values_val = values_val.transpose()
            x_val = values_val[0]
            y_val = values_val[1]

        if self.accuracy_training_gender:
            values_train = np.array(self.accuracy_training_gender)
            values_train = values_train.transpose()
            x_train = values_train[0]
            y_train = values_train[1]

        b64 = self.generate_plot([(x_train, y_train), (x_val, y_val)], 'batch', 'accuracy gender')
        return b64

    def plot_prediction(self):
        x = np.arange(100)

        b64 = list()
        for pred_age, pred_gender in self.test_image_prediction:
            for i in range(pred_age.shape[0]):
                fig = Figure()
                FigureCanvas(fig)

                ax_g = fig.add_subplot(2, 1, 1)
                ax_g.set_xticks([0, 1])
                ax_g.set_xticklabels(['Female', 'Male'])
                ax_g.bar([0, 1], pred_gender[i], 0.35)


                ax_a = fig.add_subplot(2, 1, 2)
                ax_a.plot(x, pred_age[i])
                ax_a.set_xlabel('age')
                ax_a.set_ylabel('probability')

                buff = io.BytesIO()
                fig.savefig(buff, format='svg')
                b64.append(base64.standard_b64encode(buff.getvalue()).decode('utf-8'))

        return b64

    def convert_images(self):
        ret = list()

        for img, age, gender in self.test_images:
            buff = io.BytesIO()
            img.save(buff, 'png')
            b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')


            label_a = age
            label_g = 'F' if gender[0] else 'M'

            ret.append((b64, "({:02d},{})".format(label_a, label_g)))
            #TODO: Add prediction graph to images

        return ret

    def generate_report(self):
            with open(os.path.join(__location__, 'templates/report_data.tpl.html')) as fd:
                template = Template(fd.read())

            b64svg_losses = self.plot_loss()
            b64svg_accuracy_age = self.plot_accuracy_age()
            b64svg_accuracy_gender = self.plot_accuracy_gender()
            #TODO: b64svg_gpuram = plot_vs_epochs('gpu ram', gpu_rams)
            #TODO: b64svg_lr = plot_vs_epochs('learn rate', learn_rates)
            #TODO: b64svg_time = plot_vs_epochs('time', epoch_times)

            testset_images = self.convert_images()
            b64svg_testset_prediction = self.plot_prediction()
            html = template.render(
                b64svg_losses=b64svg_losses,
                b64svg_accuracy_age=b64svg_accuracy_age,
                b64svg_accuracy_gender=b64svg_accuracy_gender,
                b64svg_testset_prediction=b64svg_testset_prediction,
                #b64svg_lr=b64svg_lr,
                #b64svg_time=b64svg_time,
                testset_images=testset_images,
                batch_size=self.batch_size,
                dataset_size=self.dataset_size,
                learning_rate=self.learning_rate,
                loss_function=self.loss_function,
                optimizer=self.optimizer
            )
            return html
            #with open(fname, 'w') as fd:
            #    fd.write(html)


class ReportGenerator(object):
    def __init__(self, path, url, args, githash):
        self.reports = list()
        self.path = path
        self.url = url
        self.start = time.time()
        self.last_update = time.time()
        self.reports = list()
        self.args = args
        self.githash = githash

    def add_report(self, report):
        self.reports.append(report)

    def get_time(self, time_):
        return time.strftime('%d.%m.%Y - %H:%M', time.localtime(time_))

    def get_time_delta(self, time_):
        return time.strftime('%H:%M', time.localtime(time_))

    def generate_html(self):
        with open(os.path.join(__location__, 'templates/report_full.tpl.html')) as fd:
            template = Template(fd.read())

        update_time = time.time()
        next_update = int(update_time + (update_time - self.last_update))
        self.last_update = update_time
        html_data = list()
        for report in self.reports:
            html_data.append(report.generate_report())

        html = template.render(
            args=self.args,
            start_time=self.get_time(self.start),
            update_time=self.get_time(update_time),
            next_time=self.get_time(next_update),
            report=html_data,
            githash=self.githash,
            epoch_time=self.get_time_delta(self.reports[-1].epoch_time_estimate)
        )

        file_name = os.path.join(self.path, 'report.html')

        with open(file_name, 'w') as fd:
            fd.write(html)

        if self.url:
            self.upload_report(os.path.join(self.path, 'report.html'), 'text/html')

    def upload_report(self, file_name, content_type):
        try:
            import requests

            with open(file_name, 'rb') as fd:
                headers = {'Content-Type': content_type}
                requests.put(self.url, data=fd, headers=headers)

        except Exception as e:
            print('e PUT failed:', e)



