#!/usr/bin/env python3

import time
import argparse
import itertools as it
import sys
import os

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.models as models

from torch.autograd import Variable
from torch.utils.data import DataLoader

from dataset import ImdbWikiDataset, SplitDataset
import helpers

class RelTime(object):
    def __init__(self):
        self.start = time.time()

    def delta(self):
        return (time.time() - self.start)

class AgeResNet(models.resnet.ResNet):
    def __init__(self, imagenet_base=None, ageresnet_base=None):
        block= models.resnet.Bottleneck

        super(AgeResNet, self).__init__(block, [3, 4, 6, 3])

        if imagenet_base:
             self.load_state_dict(torch.load(imagenet_base))

        # The size of the provided images is 160x160 pixels
        # The origignal ResNet paper uses an image size of 224x224
        # pixels.
        # The original ResNet performs two resolution halving steps
        # in the first two layers to get to 112x112 pixels and finally
        # 56x56 pixels.
        # These two layers are replaced by one that retains the original
        # size and one that scales down by a factor of three.

        # Take 160x160 images, convolve with 7x7 kernels, pad to 174x174
        # get 168x168 images
        self.conv1.stride = 1
        self.conv1.padding = 7

        # Take 168x168 images, pool with 3x3 kernel, pad to 170x170
        # get 56x56 images
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=3, padding=1)

        # The original ResNet has 1000 output classes,
        # we need only 200 for 100 ages x 2 genders
        self.fc = nn.Linear(512 * block.expansion, 200)

        self.out_act = nn.Softmax(1)

        if ageresnet_base:
            self.load_state_dict(torch.load(ageresnet_base))

        self.set_mode('test')

    def set_mode(self, mode):
        for param in self.parameters():
            param.requires_grad = False

        for param in self.select_parameters(mode):
            param.requires_grad = True

        self.mode = mode

    def select_parameters(self, mode):
        for name, param in self.named_parameters():
            if mode not in ('bake', 'preheat', 'test'):
                raise Exception('Unkown train mode {}'.format(mode))

            if (mode == 'bake') or (name in ('fc.weight', 'fc.bias')):
                yield param

    def save(self, path):
        torch.save(self.state_dict(), path)

class Trainer(object):
    def __init__(self, dataset, batch_size, network, device, mode):
        set_train, val_test_set = SplitDataset.split(dataset, 0.995, 4)
        set_val, set_test = SplitDataset.split(val_test_set, 0.80, 2)

        self.loader_train = DataLoader(set_train, batch_size=batch_size, shuffle=True, num_workers=1)
        self.loader_val = DataLoader(set_val, batch_size=batch_size)
        self.loader_test = DataLoader(set_test, batch_size=batch_size)

        self.mode = mode
        self.network = network

        self.network.set_mode(self.mode)

        self.optimizer = optim.Adam(
            self.network.select_parameters(mode),
            lr=0.01
        )

        self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, mode='min',
            factor=0.1, patience=10,
        )

        age_distribution = set_train.get_distribution()
        self.cel = nn.CrossEntropyLoss(age_distribution).to(device)

        self.device = device

    def loss_fn(self, output, labels):
        loss = self.cel(output, labels)

        return loss

    def score_fn(self, output, labels):
        _, idxes = output.detach().max(1)

        gender_scores = ((idxes / 100) == (labels / 100)).float() * 0.5
        age_scores = 0.5 - (abs((idxes % 100) - (labels % 100)).clamp(0, 10).float() / 20)

        scores = gender_scores + age_scores

        score = float(scores.mean())

        return score

    def testset_sample(self):
        with torch.no_grad():
            images_cpu, _ = next(iter(self.loader_test))
            images_gpu = images_cpu.to(self.device)

            output_gpu = self.network.forward(images_gpu)
            output_cpu = output_gpu.to('cpu')

        images = images_cpu.detach()
        output = output_cpu.detach()

        output_sm = self.network.out_act(output)

        return (images_cpu, output_sm)

    def epoch(self):
        if self.network.mode != self.mode:
            raise Exception('Network is in wrong mode')

        train_loss_acc = 0.0
        val_loss_acc = 0.0

        train_score_acc = 0.0
        val_score_acc = 0.0

        # Run through training set and update network
        for (train_batch_num, (images, labels)) in enumerate(self.loader_train):
            images = images.to(self.device)
            labels = labels.to(self.device)

            output = self.network.forward(images)
            loss = self.loss_fn(output, labels)
            score = self.score_fn(output, labels)

            del images
            del labels

            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

            train_loss = float(loss.detach())
            train_loss_acc += train_loss
            train_score_acc += score

            del loss

        # Run through validation set
        with torch.no_grad():
            for (val_batch_num, (images, labels)) in enumerate(self.loader_val):
                images = images.to(self.device)
                labels = labels.to(self.device)

                output = self.network.forward(images)
                loss = self.loss_fn(output, labels)
                score = self.score_fn(output, labels)

                del images
                del labels

                val_loss = float(loss.detach())
                val_loss_acc += val_loss
                val_score_acc += score

                del loss

        train_loss_avg = train_loss_acc / (train_batch_num + 1)
        val_loss_avg = val_loss_acc / (val_batch_num + 1)

        train_score_avg = train_score_acc / (train_batch_num + 1)
        val_score_avg = val_score_acc / (val_batch_num + 1)

        # Update learning rate
        self.scheduler.step(val_loss_avg)
        learn_rate = self.optimizer.param_groups[0]['lr']

        return (train_loss_avg, val_loss_avg, train_score_avg, val_score_avg, learn_rate)

class LogHarness(object):
    def __init__(self, outdir, url, device):
        self.timer = RelTime()

        self.train_losses = list()
        self.val_losses = list()
        self.train_scores = list()
        self.val_scores = list()
        self.learn_rates = list()
        self.gpu_rams = list()
        self.gpu_max_rams = list()
        self.epoch_times = list()

        self.device = device
        self.url = url
        self.path_best_train = os.path.join(outdir, 'best_train_loss.pth')
        self.path_best_val = os.path.join(outdir, 'best_val_loss.pth')
        self.path_report = os.path.join(outdir, 'report.html')

    def log(self, prefix, text, flush=False):
        print('{} @ {:6} - {}'.format(prefix, int(self.timer.delta() + 0.5), text), flush=flush)

    def epoch(self, trainer):
        epoch_num = len(self.train_losses) + 1

        self.log('p', 'Run Epoch: {}'.format(epoch_num), True)
        train_loss, val_loss, train_score, val_score, learn_rate = trainer.epoch()

        gpu_max_ram = torch.cuda.max_memory_allocated(self.device.index) if self.device.type != 'cpu' else 0
        gpu_ram = torch.cuda.memory_allocated(self.device.index) if self.device.type != 'cpu' else 0

        self.log('t', 'Loss: {}'.format(train_loss))
        self.log('v', 'Loss: {}'.format(val_loss))

        self.train_losses.append(train_loss)
        self.val_losses.append(val_loss)
        self.train_scores.append(train_score)
        self.val_scores.append(val_score)
        self.learn_rates.append(learn_rate)
        self.gpu_max_rams.append(gpu_max_ram)
        self.gpu_rams.append(gpu_ram)
        self.epoch_times.append(self.timer.delta())

        if train_loss <= min(self.train_losses):
            trainer.network.save(self.path_best_train)

        if val_loss <= min(self.val_losses):
            trainer.network.save(self.path_best_val)

        test_images, test_labels = trainer.testset_sample()

        helpers.gen_report(
            self.path_report,
            self.train_losses, self.val_losses,
            self.train_scores, self.val_scores,
            self.gpu_rams, self.gpu_max_rams, self.learn_rates,
            test_images, test_labels,
            ' '.join(sys.argv),
            list(e - s for (e, s) in zip(self.epoch_times, [0] + self.epoch_times[:-1])),
        )

        if self.url:
            helpers.put_file(self.path_report, 'text/html', self.url)

        return (train_loss, val_loss, learn_rate)


def find_accelerator():
    use_cuda = torch.cuda.is_available()
    device = torch.device('cuda' if use_cuda else 'cpu')

    return device


def main():
    parser = argparse.ArgumentParser(description='Group 6 AgeResNet trainer')

    parser.add_argument('-b', '--minibatch', help='Minibatch size', required=False, default=256, type=int)
    parser.add_argument('-l', '--limit', help='Limit Dataset to n images', required=False, default=0, type=int)
    parser.add_argument('-i', '--imagenet', help='Imagenet base network', required=False, default='', type=str)
    parser.add_argument('-a', '--ageresnet', help='AgeResNet base network', required=False, default='', type=str)
    parser.add_argument('-d', '--dataset', help='Dataset Database', required=False, default='meta.db', type=str)
    parser.add_argument('-u', '--url', help='URL to PUT reports to', required=False, default=None, type=str)
    parser.add_argument('-o', '--outdir', help='Output directory', required=False, default='.', type=str)
    parser.add_argument('-p', '--preheat', help='Preheating Epoch limit', required=False, default=100, type=int)
    parser.add_argument('-e', '--epochs', help='Epoch limit', required=False, default=400, type=int)
    parser.add_argument('-g', '--git', help='Git Commit hash', required=False, default='', type=str)

    args = parser.parse_args()

    device = find_accelerator()
    network = AgeResNet(args.imagenet, args.ageresnet).to(device)
    dataset = ImdbWikiDataset(args.dataset)
    log_harness = LogHarness(args.outdir, args.url, device)

    if args.limit != 0:
        # Reduce dataset size for testing purposes
        dataset = SplitDataset(dataset, 0, args.limit)

    log_harness.log('s', 'Using device: {}, Batch size: {}'.format(device, args.minibatch))

    if args.imagenet:
        # Train only the fully connected output layer at first
        log_harness.log('s', 'Start preheating')
        trainer_preheat = Trainer(dataset, args.minibatch, network, device, 'preheat')

        for epoch_num in range(args.preheat):
            _, _, learn_rate = log_harness.epoch(trainer_preheat)

            if learn_rate <= 1e-5:
                break


    # Next train the whole network
    log_harness.log('s', 'Start baking')
    trainer_bake = Trainer(dataset, args.minibatch, network, device, 'bake')

    for epoch_num in range(args.epochs):
        _, _, learn_rate = log_harness.epoch(trainer_bake)

        if learn_rate <= 1e-6:
                break

if __name__ == '__main__':
    main()
