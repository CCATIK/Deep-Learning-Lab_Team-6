#!/bin/bash
#SBATCH --job-name=AGENET
#SBATCH --gres=gpu:GTX1080Ti:1
#SBATCH --partition=dll
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=10gb
#SBATCH --time=100:00:00

# Necessary call, don't change
source /etc/profile >> /dev/null

echo "Computing job "${SLURM_JOB_ID}" on "$(hostname)

username=$(whoami)
script_name="train.py"
dir_scripts="/home/${username}/code/repo/solutions/part4/ResNet1/"
dir_output="/home/${username}/code/repo/solutions/part4/ResNet1/out_${SLURM_JOB_ID}"
gitpath=$(cat /home/${username}/code/repo/.git/HEAD | awk '{ print $2}')
githash=$(cat /home/${username}/code/repo/.git/${gitpath})

mkdir ${dir_output}

# Load modules and environment
module load anaconda/3-5.0.1
module load cuda/9.1
source activate trainenv

# Start the job
srun python3 ${dir_scripts}${script_name} -o ${dir_output} -g ${githash} ${@}
