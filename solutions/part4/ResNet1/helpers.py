import io
import base64
import time

import numpy as np

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from jinja2 import Template

from torchvision import transforms

img_process = transforms.Compose([
    transforms.Normalize(
        mean=[-0.485/0.229, -0.456/0.224, -0.406/0.225],
        std=[1/0.229, 1/0.224, 1/0.225]
    ),
    transforms.ToPILImage(),
])

def plot_vs_epochs(label, *values):
    x = np.arange(len(values[0])) + 1

    y_s = list(np.array(value) for value in values)

    fig = Figure()
    FigureCanvas(fig)

    dims = [x, None] * len(y_s)
    dims[1::2] = y_s

    ax = fig.add_subplot(1, 1, 1)
    ax.plot(*dims)
    ax.set_xlabel('epochs')
    ax.set_ylabel(label)

    buff = io.BytesIO()
    fig.savefig(buff, format='svg')
    b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

    return b64

def plot_ages(male, female):
    x = np.arange(len(male))

    fig = Figure()
    FigureCanvas(fig)

    ax = fig.add_subplot(1, 1, 1)
    ax.plot(x, male, x, female)
    ax.set_xlabel('age')
    ax.set_ylabel('probability')

    buff = io.BytesIO()
    fig.savefig(buff, format='svg')
    b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

    return b64

def convert_images(test_images, test_guess):
    ret = list()

    for i in range(test_images.shape[0]):
        img = img_process(test_images[i,:,:])

        buff = io.BytesIO()
        img.save(buff, 'png')
        b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

        male = test_guess[i,:100].detach().numpy()
        female = test_guess[i,100:].detach().numpy()
        guess = plot_ages(male, female)

        ret.append((b64, guess))

    return ret

def format_ts(ts):
    fmt = time.strftime('%d.%m.%Y - %H:%M', time.localtime(ts))

    return fmt

def gen_report(fname, train_losses, val_losses, train_scores, val_scores,
               gpu_rams, gpu_max_rams, learn_rates,
               test_images, test_guess, args, epoch_times):
    with open('report.tpl.html') as fd:
        template = Template(fd.read())

    b64svg_losses = plot_vs_epochs('loss', train_losses, val_losses)
    b64svg_scores = plot_vs_epochs('score', train_scores, val_scores)
    b64svg_gpuram = plot_vs_epochs('gpu ram', gpu_rams, gpu_max_rams)
    b64svg_lr = plot_vs_epochs('learn rate', learn_rates)
    b64svg_time = plot_vs_epochs('time', epoch_times)

    testset_images = convert_images(test_images, test_guess)

    ts_now = time.time()
    ts_next = ts_now + epoch_times[-1]

    html = template.render(
        b64svg_losses=b64svg_losses,
        b64svg_scores=b64svg_scores,
        b64svg_gpuram=b64svg_gpuram,
        b64svg_lr=b64svg_lr,
        b64svg_time=b64svg_time,
        testset_images=testset_images,
        args=args,
        ts_now=format_ts(ts_now),
        ts_next=format_ts(ts_next),
    )

    with open(fname, 'w') as fd:
        fd.write(html)

def put_file(fname, content_type, url):
    try:
        import requests

        with open(fname, 'rb') as fd:
            headers = {'Content-Type' : content_type}
            requests.put(url, data=fd, headers=headers)

    except Exception as e:
        print('e PUT failed:', e)
