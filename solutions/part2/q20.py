#!/usr/bin/env python

# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- (1) HEADERS --------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

# This script generates and imports datasets for various dimensions, trains Support Vector Machines with previously set
# parameters. Results are displayed in dimensions d=2 and d=3 and outputs the classification accuracy gained with the trained model

__author__ = "Markus Kamp"
__copyright__ = "Copyright 2017, Institute for Communications Technology (IfN), TU Braunschweig"
__version__ = "1.1.2"
__maintainer__ = "Markus Kamp"
__email__ = "m.kamp@tu-bs.de"
__status__ = "Pre-Release"

# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- (2) IMPORTS --------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

# Colormap Import
import matplotlib.colors as mpc

# 2D Imports for scikit
from sklearn import svm

# Random data generators
from svm_modules.svm_data_generator import *

# 2D-Plotting
from svm_modules.svm_2d_plotting import *

# Utilities
from svm_modules.svm_util import *

# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- (3) SVM PARAMETERS -------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

## General
predefined_train =          False   # Use predefined dataset for training (bool, default: False)
use_banana =                False   # Use preset banana dataset for training/test (bool, default: False)
use_test_data =             False   # Generate data for test and calc accuracy on this set too (bool, default: False)
predefined_test =           False   # Use predefined dataset for test (bool, default: False)
show_support_vectors =      True    # Show support vectors in 2D graph (bool, default: True)
gen_Mode =                  'roll' # Determines the data generator; 'gauss' for gaussian generator,
                                    # 'roll' for swiss roll (2D) helix generator (string, default: 'gauss')
seed_gen =                  110     # Seed for random data generation (training and test, all classes)
                                    # 0: No seed used (random), > 0: Seed used (int, default: 0)
resolution_grid_plot =      500     # Resolution for scatter grid used in 2D plotting (int, default: 500)
plot_border_margin =        0.1     # Margin between plots edge and nearest data vector in 2D (float, default: 0.1)
cache =                     4000    # Cache size (in MB) for SVM training, adjust to your system memory size
                                    # (float, default: 4000)

# ----------------------------------------- TRAINING PARAMETERS ------------------------------------------------------ #

### Training

## Random data generation

# Random gaussian data generation

if seed_gen > 0:
    np.random.seed(seed_gen)

amount_samples_train = np.array([100])                     # Amount samples for training (Generator)
means_train = np.array([[-2, 2,],
                        [2, -2],
                        [-2, -2],
                        [2, 2]])   # Array, consists out of mean vectors with same dimension, e.g. [[2,4,3],[2,4,2]] for two 3-dimensionals

#   Array, consists out of corresponding covariance matrices, e.g. [[[1,0,-2],[2,1,0],[3,2,1]],[[1,0,-2],[2,1,0],[3,2,1]]]
#   for two 3-dimensionals

covs_train = np.array([[[1,0], [0,1]],
                       [[1,0], [0,1]],
                       [[1,0], [0,1]],
                       [[1, 0], [0, 1]]])

# Array, consists out of labels for data clusters, e.g. [0,1] for two clusters with labels for class 0 and 1
# If labels shall be generated automatically, just let array empty! -> np.array([])
labels_train = np.array([0, 0, 1, 1])

# Random swiss roll data generation

kernel_coeff = 10

amount_samples_train_swiss = np.array([750,750])    # Amount of Samples per generated Helix

roll_noise_train = np.array([0.25,0.25])            # Noise if the generated data

seed_swiss_train = np.array([110,110])              # Preset seed for generation

roll_factors_train = np.array([1.5])               # Factor to scale helix (e.g. 1.5 times smaller compared to
                                                    # previous helix

## Predefined data train

if use_banana == True:
    predefine_file_train = "..\\data\\banana_lib.dat"   # File with banana predefined data
    predefined_train = True                             # Take care, that predifined dataset is chosen
else:
    predefine_file_train = "..\\data\\a.dat"            # File with predefined data

### SVM-Training

kernel_function =           'rbf'       # Specifies the kernel type to be used in the algorithm
                                        # It must be one of 'linear', 'poly', 'rbf', 'sigmoid'
                                        # (string, default: 'rbf')
stopping_criterion =        1e-3        # Tolerance for stopping criterion (float, default: 1e-3)
max_iterations =            -1          # Hard limit on iterations within solver, or -1 for no limit (int, default: -1)

# ----------------------------------------- TEST PARAMETERS ---------------------------------------------------------- #

### Test

## Random data generation

# Random gaussian data generation

amount_samples_test = np.array([100])               # Amount samples for test (Generator)
means_test = np.array([[-2,
                         2],

                        [2,
                        -2]])           # Array, consists out of mean vectors with same dimension, e.g. [[2,4,3],[2,4,2]] for two 3-dimensionals

#   Array, consits out of corresponding covariance matrices, e.g. [[[1,0,-2],[2,1,0],[3,2,1]],[[1,0,-2],[2,1,0],[3,2,1]]]
#   for two 3-dimensionals

covs_test = np.array([[[1,0],
                       [0,1]],

                      [[1,0],
                       [0,1]]])

# Array, consists out of labels for data clusters, e.g. [0,1] for two clusters with labels for class 0 and 1
# If labels shall be generated automatically, just let array empty! -> np.array([])

labels_test = np.array([0,1])

# Random swiss roll data generation

amount_samples_test_swiss = np.array([750,750])    # Amount of Samples per generated Helix

roll_noise_test = np.array([0.25,0.25])            # Noise if the generated data

seed_swiss_test = np.array([110,110])              # Preset seed for generation

roll_factors_test = np.array([1.5])                # Factor to scale helix (e.g. 1.5 times smaller compared to
                                                   # previous helix

## Predefined data test

predefine_file_test = 'data_a1.test'    # File with predefined data

### Define colormap

# -------------------------------------------------------------------------------------------------------------------- #
# ----------------------------------------- (4) FUNCTION DEFINITIONS ------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

# No functions to define this time

# -------------------------------------------------------------------------------------------------------------------- #
# ----------------------------------------- (5) CREATE/IMPORT DATASETS ----------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

### Generate a random n-class classification problem.

if predefined_train == False:
    print("Generate Training Data Set: {sample} samples\n".format(sample=amount_samples_train))
    if gen_Mode == 'gauss':
        X, Y = genGauss(means_train, covs_train, labels_train, amount_samples_train)
    elif gen_Mode == 'roll':
        X, Y = genSwiss(amount_samples_train_swiss, roll_noise_train, seed_swiss_train, roll_factors_train)
    else:
        exit("Error:\tWrong gen_Mode has been set. Please set to 'gauss' or 'roll'!")
    number_classes = len(set(Y))
    dimension_data = X.shape[1]
else:
    print("Import data for training\n")
    #TODO: print-Ausgabe: wieviele samples wurden geladen?
    X, Y = loadDatasets(predefine_file_train)
    number_classes = len(set(Y))
    dimension_data = X.shape[1]

#TODO: Hier ein Kommentar: Was genau ist Y. Vektor mit Labels?
Y = mapLabels(Y, True)

# Check for invalid inputs
if (predefined_test == True or predefined_train == True):
    print("You are using extern data import, pay attention that the dimension_data parameter fits to the data\n")
    print("which you want to import! Otherwise error will occur!\n")

if (number_classes > 9) | (number_classes < 2):
    exit('Error:\tAmount of data classes: 1 < number_classes < 10!\n\t\tAdjust value in the properties section!\n')

if (dimension_data < 2) | (dimension_data > 3):
    print("You have set dimension to a non-representable value. There will only be text output!\n")

# Test data
if use_test_data == True:
    if predefined_test == False:
        print("Generate Test Data Set: {sample} samples\n".format(sample=amount_samples_test))
        if gen_Mode == 'gauss':
            XT, YT = genGauss(means_test, covs_test, labels_test, amount_samples_test)
        elif gen_Mode == 'roll':
            XT, YT = genSwiss(amount_samples_test_swiss, roll_noise_test, seed_swiss_test, roll_factors_test)
        else:
            exit("Error:\tWrong gen_Mode has been set. Please set to 'gauss' or 'roll'!")
    else:
        if use_banana == True:
            print("Split banana data in training and test set\n")
            X, Y, XT, YT = split_tr_dev(X, Y, 0.9)
        else:
            print("Import data for test\n")
            XT, YT = loadDatasets(predefine_file_test)
    YT = mapLabels(YT, False)

# -------------------------------------------------------------------------------------------------------------------- #
# ----------------------------------------- (6) TRAINING OF SVM ------------------------------------------------------ #
# -------------------------------------------------------------------------------------------------------------------- #

if dimension_data == 2:

    ## 2D Visuals

    cmapCust = mpc.ListedColormap(cmapArray)

    if use_test_data == False:
        plt.title("Data for training", fontsize='medium')
        train_adj = adjustWindow(X, dimension_data, plot_border_margin)
        pltWindow(X, Y, train_adj[0], train_adj[2], train_adj[1], train_adj[3], 30, 0, number_classes, cmapCust)
        plt.legend(loc = 4)
        #tikz_save('test.tex')
        plt.show()
        plt.clf()
    else:
        plt.subplot(1, 2, 1)
        #plt.subplots_adjust(wspace=5.0, hspace=5.0)

        plt.title("Data for training", fontsize='medium')
        train_adj = adjustWindow(X, dimension_data, plot_border_margin)
        pltWindow(X, Y, train_adj[0], train_adj[2], train_adj[1], train_adj[3], 30, 0, number_classes, cmapCust)
        plt.legend(loc = 4)

        plt.subplot(1, 2, 2)

        plt.title("Data for test", fontsize='medium')
        test_adj = adjustWindow(XT, dimension_data, plot_border_margin)
        pltWindow(XT, YT, train_adj[0], train_adj[2], train_adj[1], train_adj[3], 30, 0, number_classes, cmapCust)
        plt.legend(loc = 4)
        plt.show()
        plt.clf()

    # Check if training shall be performed
    # Create meshgrid for plot
    xx, yy = np.meshgrid(np.linspace(train_adj[0], train_adj[2], resolution_grid_plot),
                         np.linspace(train_adj[1], train_adj[3], resolution_grid_plot))

    # fit the model
    clf = svm.SVC(kernel=kernel_function, tol=stopping_criterion, max_iter=max_iterations,
                  decision_function_shape='ovr', cache_size=cache, gamma=kernel_coeff)
    print("Train SVM Model\n")
    clf.fit(X, Y)

    # Plotting 2D Results

    # plot the decision function for each datapoint on the grid
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    acc_str_train = ('Result SVM-Training - Accuracy on training data = ' + '{0:g}%'.format(clf.score(X, Y) * 100) + "\n")
    if use_test_data == True:
        acc_str_test = ('Result SVM-Training - Accuracy on test data = ' + '{0:g}%'.format(clf.score(XT, YT) * 100) + "\n")

    try:
        if use_test_data == False:
            plt.pcolormesh(xx, yy, Z, cmap=cmapCust)
            contours = plt.contour(xx, yy, Z, linewidths=1, colors='black', linetypes='--')
            pltWindow(X, Y, train_adj[0], train_adj[2], train_adj[1], train_adj[3], 30, 10, number_classes, cmapCust)
            plt.title(acc_str_train)
            if show_support_vectors == True:
                plt.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=60, marker='s', facecolors='none',
                            zorder=10,
                            edgecolors='black', label='Support Vectors')
            plt.legend(loc=4)
            plt.show()
        else:
            plt.subplot(1, 2, 1)

            plt.pcolormesh(xx, yy, Z, cmap=cmapCust)
            contours = plt.contour(xx, yy, Z, linewidths=1, colors='black', linetypes='--')
            if show_support_vectors == True:
                plt.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=60, marker='s', facecolors='none', zorder=10,
                            edgecolors='black', label = 'Support Vectors')
            plt.title(acc_str_train)
            pltWindow(X, Y, train_adj[0], train_adj[2], train_adj[1], train_adj[3], 30, 10, number_classes, cmapCust)
            plt.legend(loc = 4)
            plt.subplot(1, 2, 2)

            plt.pcolormesh(xx, yy, Z, cmap=cmapCust)
            contours = plt.contour(xx, yy, Z, linewidths=1, colors='black', linetypes='--')
            reg = np.zeros(number_classes)
            ys = [i + XT + (i * XT) ** 2 for i in range(number_classes)]
            colors = cmapCust(np.linspace(0, 1, len(ys)))
            for xp, c, m in zip(XT, YT, markMap(YT)):
                if reg[c.astype(int)] == 0:
                    plt.scatter(xp[0], xp[1], s=30, c=colors[c.astype(int)], marker=m, facecolors='none', zorder=10,
                                edgecolors='black', label='Class ' + '{0:g}'.format(c.astype(int)))
                    reg[c.astype(int)] = 1
                else:
                    plt.scatter(xp[0], xp[1], s=30, c=colors[c.astype(int)], marker=m, facecolors='none', zorder=10,
                                edgecolors='black')
            plt.title(acc_str_test)
            plt.axis([train_adj[0], train_adj[2], train_adj[1], train_adj[3]])  # [low_x, high_x, low_y, high_y]
            plt.xlabel('X-Dimension')
            plt.ylabel('Y-Dimension')
            plt.legend(loc = 4)
            plt.show()
    except ValueError:
        print(acc_str_train)
        if use_test_data == True:
            print(acc_str_test)
        exit('Error:\tData is hard to seperate!\n\t\tPlease use other kernel like rbf.\n')
else:
    exit('Error:\tDimension of generated data is not in 2d space!\n\t\tAdjust values in the properties section!\n')

print("Training finished successfully, closing application!")