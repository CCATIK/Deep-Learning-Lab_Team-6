#!/usr/bin/env python

# This script imports MNIST dataset, trains SVM and gives the accuracy gained with the trained model

__author__ = "Markus Kamp"
__copyright__ = "Copyright 2017, Institute for Communications Technology (IfN), TU Braunschweig"
__version__ = "1.1.0"
__maintainer__ = "Markus Kamp"
__email__ = "m.kamp@tu-bs.de"
__status__ = "Released"

import timeit

def init_train(type_, size_):
    import numpy as np

    # 2D Imports for scikit



    # MNIST Imports

    from mnist import MNIST

    ## Parameter for SVM-Training

    ## General

    global use_test_data
    use_test_data = 1                       # 1: Accuracy on train + test data; 0: Accuracy on train data
    mnist_two_case = 1                      # Use only a two case problem (digits 3 and 8) (1: two case, 0: multi case)
    use_dev = 1                             # 1: Use dev as "test" data set to optimize parameters 0: Use test set for
                                            # performance test (int, default: 1)
    data_reduction = 1                      # Reduce data set size; 1 = reduction, 0 = no reduction
    global red_set_size
    red_set_size = size_                     # Size of reduced set
    global cache
    cache = 4000                            # Cache size (in MB) for SVM training, adjust to your system memory size (float, default: 4000)
    global training_method
    training_method = type_                 # Switch between svm and nnet training

    ## Training

    # SVM-Training
    global penalty
    penalty = 1.0                           # Penalty parameter C of the error term (float, default: 1.0)
    global kernel_function
    kernel_function = 'linear'              # Specifies the kernel type to be used in the algorithm
                                            # It must be one of 'linear', 'poly', 'rbf', 'sigmoid'
                                            # (string, default: 'rbf')
    global poly_degree
    poly_degree = 3                         # Degree of the polynomial kernel function ('poly')
                                            # Ignored by all other kernels
                                            # (int, default: 3)
    global kernel_coeff
    kernel_coeff = 'auto'                   # Kernel coefficient for 'rbf', 'poly' and 'sigmoid'
                                            # If gamma is 'auto' then 1/n_features will be used instead
                                            # (float, default: 'auto'
    global zero_coeff
    zero_coeff = 0.0                        # Independent term in kernel function
                                            # It is only significant in 'poly' and 'sigmoid'
                                            # (float, default: 0.0)
    global stopping_criterion
    stopping_criterion = 1e-3               # Tolerance for stopping criterion (float, default: 1e-3)
    global max_iterations
    max_iterations = -1                     # Hard limit on iterations within solver, or -1 for no limit (int, default: -1)

    # NNET-Training

    global hidden_layer_sizes
    hidden_layer_sizes =    (100, 100, 100, )         # tuple, length = n_layers - 2, default (100,)
                                            # The ith element represents the number of neurons in the ith hidden layer.
    global activation
    activation =            "tanh"          # {‘identity’, ‘logistic’, ‘tanh’, ‘relu’}, default ‘relu’
                                            # Activation function for the hidden layer.
                                            # ‘identity’, no-op activation, useful to implement linear bottleneck, returns f(x) = x
                                            # ‘logistic’, the logistic sigmoid function, returns f(x) = 1 / (1 + exp(-x)).
                                            # ‘tanh’, the hyperbolic tan function, returns f(x) = tanh(x).
                                            # ‘relu’, the rectified linear unit function, returns f(x) = max(0, x)
    global solver
    solver =                "sgd"           # {‘lbfgs’, ‘sgd’, ‘adam’}, default ‘adam’
                                            # The solver for weight optimization.
                                            # ‘lbfgs’ is an optimizer in the family of quasi-Newton methods.
                                            # ‘sgd’ refers to stochastic gradient descent.
                                            # ‘adam’ refers to a stochastic gradient-based optimizer proposed by Kingma, Diederik, and Jimmy Ba
                                            # Note: The default solver ‘adam’ works pretty well on relatively large datasets
                                            # (with thousands of training samples or more) in terms of both training time and validation score.
                                            # For small datasets, however, ‘lbfgs’ can converge faster and perform better.
    global alpha
    alpha =                 0.0001          # float, default 0.0001
                                            # L2 penalty (regularization term) parameter.
    global batch_size
    batch_size =            "auto"          # int, default ‘auto’
                                            # Size of minibatches for stochastic optimizers. If the solver is ‘lbfgs’,
                                            # the classifier will not use minibatch. When set to “auto”, batch_size=min(200, n_samples)
    global learning_rate
    learning_rate =         "constant"      # {‘constant’, ‘invscaling’, ‘adaptive’}, default ‘constant’
                                            # Learning rate schedule for weight updates.
                                            # ‘constant’ is a constant learning rate given by ‘learning_rate_init’.

                                            # ‘invscaling’ gradually decreases the learning rate learning_rate_ at each time step ‘t’ using an
                                            # inverse scaling exponent of ‘power_t’. effective_learning_rate = learning_rate_init / pow(t, power_t)

                                            # ‘adaptive’ keeps the learning rate constant to ‘learning_rate_init’ as long as training
                                            # loss keeps decreasing. Each time two consecutive epochs fail to decrease training loss by at least tol,
                                            # or fail to increase validation score by at least tol if ‘early_stopping’ is on, the current learning rate is divided by 5.

                                            # Only used when solver='sgd'.
    global learning_rate_init
    learning_rate_init =    0.001           # double, default 0.001
                                            # The initial learning rate used. It controls the step-size in updating the weights. Only used when solver=’sgd’ or ‘adam’.
    global power_t
    power_t =               0.5             # double, default 0.5
                                            # The exponent for inverse scaling learning rate. It is used in updating effective
                                            # learning rate when the learning_rate is set to ‘invscaling’. Only used when solver=’sgd’.
    global max_iter
    max_iter =              200             # int, default 200
                                            # Maximum number of iterations. The solver iterates until convergence (determined by ‘tol’) or this number of iterations.
                                            # For stochastic solvers (‘sgd’, ‘adam’), note that this determines the number of
                                            # epochs (how many times each data point will be used), not the number of gradient steps.
    global shuffle
    shuffle =               True            # bool, default True
                                            # Whether to shuffle samples in each iteration. Only used when solver=’sgd’ or ‘adam’.
    global random_state
    random_state =          None            # int, RandomState instance or None, default None
                                            # If int, random_state is the seed used by the random number generator; If RandomState instance,
                                            # random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.
    global tol
    tol =                   0.0001          # float, default 1e-4
                                            # Tolerance for the optimization. When the loss or score is not improving by at least tol for
                                            # two consecutive iterations, unless learning_rate is set to ‘adaptive’, convergence is considered to be reached and training stops.
    global verbose
    verbose =               False           # bool, default False
                                            # Whether to print progress messages to stdout.
    global warm_start
    warm_start =            False           # bool, default False
                                            # When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution.
    global momentum
    momentum =              0.9             # float, default 0.9
                                            # Momentum for gradient descent update. Should be between 0 and 1. Only used when solver=’sgd’.
    global nesterovs_momentum
    nesterovs_momentum =    True            # boolean, default True
                                            # Whether to use Nesterov’s momentum. Only used when solver=’sgd’ and momentum > 0.
    global early_stopping
    early_stopping =        False           # bool, default False
                                            # Whether to use early stopping to terminate training when validation score is not improving. If set to true, it will automatically set
                                            # aside 10% of training data as validation and terminate training when validation score is not improving by at least tol for
                                            # two consecutive epochs. Only effective when solver=’sgd’ or ‘adam’
    global validation_fraction
    validation_fraction =   0.1             # float, default 0.1
                                            # The proportion of training data to set aside as validation set for early stopping.
                                            # Must be between 0 and 1. Only used if early_stopping is True
    global beta_1
    beta_1 =                0.9             # float, default 0.9
                                            # Exponential decay rate for estimates of first moment vector in adam, should be in [0, 1). Only used when solver=’adam’
    global beta_2
    beta_2 =                0.999           # float, default 0.999
                                            # Exponential decay rate for estimates of second moment vector in adam, should be in [0, 1). Only used when solver=’adam’
    global epsilon
    epsilon =               1e-08           # float, default 1e-8
                                            # Value for numerical stability in adam. Only used when solver=’adam’

    ## Define functions

    # Returns ndarray with only data representing digits 3 and 8
    # Return: images (data), labels [ndarray]
    def get_3_8(images_l, labels_a):
        images = np.asarray(images_l)
        labels = np.asarray(labels_a)
        image_3_8 = []
        labels_3_8 = []
        for i in range(0, labels.size):
            if ((labels[i] == 3) or (labels[i] == 8)):
                image_3_8.append(images[i])
                labels_3_8.append(labels[i])

        return np.asarray(image_3_8), np.asarray(labels_3_8)

    # Splits train set into train and dev with proportion parameter
    # Returns: images_train, labels_train, images_dev, labels_dev [ndarray]
    # Proportion Parameter: (0.0 [ALL DEV] ... 1.0 [ALL TRAIN]) (float),
    def split_tr_dev(images, labels, proportion_train):
        splitter = np.ceil(labels.size * proportion_train).astype(int)
        return images[0:splitter], labels[0:splitter], images[splitter:labels.size], labels[splitter:labels.size]

    # Reduces the full mnist two-case set to a better trainable subset
    # For numbers not dividable by 2 (e.g. 1, 3) the set size will be rounded up
    # Parameters:
    # X = ndarray of data point values
    # Y = ndarray of labels for data points
    # set_dim = dimension of reduced data set (int, default: 500)

    def reduceSet(X, Y, set_dim):
        if (set_dim >= X.shape[0]):
            #print("\tNo reduction, set size already greater\n")
            return X, Y
        if (set_dim < 0):
            #print("\tNo reduction, set size too small (< 0)\n")
            return X, Y
        set_dim = np.ceil(set_dim / 2).astype(int)
        X_N = []
        Y_N = []
        iter_3, iter_8 = 0,0
        for i in range(0, X.size):
            if iter_3 < set_dim:
                if Y[i] == 3:
                    X_N.append(X[i])
                    Y_N.append(Y[i])
                    iter_3 += 1
            if iter_8 < set_dim:
                if Y[i] == 8:
                    X_N.append(X[i])
                    Y_N.append(Y[i])
                    iter_8 += 1
            if ((iter_3 == set_dim) and (iter_8 == set_dim)):
                break
        return np.asarray(X_N), np.asarray(Y_N)


    # Import and parse MNIST database

    #print("Import MNIST\n")
    mndata = MNIST('../data')

    # Load data for training
    global X, Y, XT, YT
    X, Y = mndata.load_training()
    if (mnist_two_case == 1):
        #print("Generate two case problem (training)\n")
        X, Y = get_3_8(X, Y)

        if data_reduction == 1:
            # Reduce data set size for better trainability
            #print("Reduce data set size\n")
            X, Y = reduceSet(X, Y, red_set_size)

    # Load data for dev/test

    if use_test_data == 1:
        if (use_dev == 1):
            #print("Split MNIST training set in training and dev set\n")
            X, Y, XT, YT = split_tr_dev(X, Y, 0.7)  # More information about parameters: see function decription
        else:
            #print("Import MNIST test data\n")
            XT, YT = mndata.load_testing()
            if (mnist_two_case == 1):
                #print("Generate two case problem (test)\n")
                XT, YT = get_3_8(XT, YT)
                if data_reduction == 1:
                    # Reduce data set size for better trainability
                    # print("Reduce data set size\n")
                    XT, YT = reduceSet(XT, YT, np.ceil(red_set_size / 10).astype(int))

    # Train SVM and calculate ACC

def train_model():
    from sklearn import svm
    from sklearn import neural_network
    if training_method == 'svm':
        print("svm, {}:".format(red_set_size))
        clf = svm.SVC(C=penalty, kernel=kernel_function, degree=poly_degree, gamma=kernel_coeff, coef0=zero_coeff,
                      tol=stopping_criterion, max_iter=max_iterations, decision_function_shape='ovr', cache_size=cache)
    elif training_method == 'nnet':
        print("nnet, {}:".format(red_set_size))
        clf = neural_network.MLPClassifier(hidden_layer_sizes, activation, solver, alpha, batch_size, learning_rate,
                                           learning_rate_init, power_t, max_iter, shuffle, random_state, tol, verbose,
                                           warm_start, momentum, nesterovs_momentum, early_stopping,
                                           validation_fraction,
                                           beta_1, beta_2, epsilon)
    else:
        exit("Error: No valid method for training has been set!\nPlease set method to svm or nnet.")

    # print("Train SVM Model\n")
    clf.fit(X, Y)

    print('Accuracy on training data = ' + '{0:g}%'.format(clf.score(X, Y) * 100))

    if use_test_data == 1:
        print('Accuracy on test data = ' + '{0:g}%'.format(clf.score(XT, YT) * 100))

    #print("Training finished successfully, closing application!")


train_types = ['nnet', 'svm']
size = [25, 50, 75, 100, 150, 200, 300, 500, 750, 1000, 3000, 6000]


for s in size:
    for t_type in train_types:
        g = lambda : init_train(t_type, s)
        print(timeit.timeit(train_model, setup = g, number = 5))