# CNTK MNIST
# Deep Learning Lab - IfN 2017
# ==============================================================================

# Import libraries
from __future__ import print_function
import os
import sys

import math
import argparse
import numpy as np

# Extend python search path for utils dir
sys.path.append(os.path.expanduser("~") + "/utils")

import labreport
import cntk
import _cntk_py
import cntk.io.transforms as xforms
from cntk.io import MinibatchSource, ImageDeserializer, StreamDef, StreamDefs, INFINITELY_REPEAT, FULL_DATA_SWEEP, CTFDeserializer
from cntk.layers import Convolution2D, MaxPooling, AveragePooling, Dropout, BatchNormalization, Dense, default_options, identity, Sequential, For

from cntk.train.training_session import *
from cntk.logging import *
from cntk.debugging import *
from cntk.initializer import *

from cntk.layers.typing import *
from cntk import Trainer
from cntk.learners import momentum_sgd, learning_rate_schedule, UnitType, momentum_as_time_constant_schedule
from cntk import cross_entropy_with_softmax, classification_error, relu
from cntk.ops import Function
from cntk.debugging import set_computation_network_trace_level

from cntk.device import try_set_default_device, gpu
try_set_default_device(gpu(0))
from _cntk_py import set_fixed_random_seed, force_deterministic_algorithms
set_fixed_random_seed(1)
force_deterministic_algorithms()

np.random.seed(12345)

def str2bool(v):
	if v.lower() in ('yes', 'true', 't', 'y', '1'):
		return True
	elif v.lower() in ('no', 'false', 'f', 'n', '0'):
		return False
	else:
		raise argparse.ArgumentTypeError('Boolean value expected.')

# Set variables
parser = argparse.ArgumentParser(description='This script is used to train a CNN/FFN for MNIST image recognition. The following	 parameters can be assigned:')
parser.add_argument('-mb', '--minibatch', help='Minibatch size', required=False, default='64', type=int)
parser.add_argument('-do','--dropout',help='Dropout value', required=False, default='0.0', type=float)
parser.add_argument('-l2','--l2weight',help='L2 regularization weight', required=False, default='0.002', type=float)
parser.add_argument('-lr','--learningrate',help='Learning rate (schedule)', required=False, default='[0.0015625]*20+[0.00046875]*20+[0.00015625]*20+[0.000046875]*10+[0.000015625]')
parser.add_argument('-mo','--momentum',help='Momentum (schedule)', required=False, default='[0]*20+[600]*20+[1200]')
parser.add_argument('-me','--maxepochs',help='Number of training epochs', required=False, default='80', type=int)
parser.add_argument('-eps','--epochsize',help='Size of training epoch', required=False, default='50000', type=int)
parser.add_argument('-log','--logfile',help='Save trainer output to logfile', required=False, default='logfile.txt')
parser.add_argument('-aug','--dataaug',help='Use data augmentation', required=False, default=False, type=str2bool)
parser.add_argument('-data', '--datapath', help='Provide input data path', required=True)
parser.add_argument('-db', '--database', help='Provide database name', required=True)
parser.add_argument('-out', '--output', help='Absolute path to output directory', required=True)
parser.add_argument('-nt', '--nettype', help='Type of network (cnn/ffn)', required=True)
parser.add_argument('-pt', '--part', help='Lab part: 3 or 4', required=True, type=int)

args = parser.parse_args()

exec('lerningrate={}'.format(args.learningrate))
exec('momentum={}'.format(args.momentum))

# Set model dimensions
image_height = 28
image_width	 = 28
num_channels = 1
num_classes	 = 10
input_dim = image_height * image_width * num_channels
if args.nettype == "ffn":
	num_hidden_layers = 2
	hidden_layers_dim = 400

def main():
	# Create the model (1) and (2)
	model = create_network_and_criterion()	  
	
	# Create readers (3)
	reader_train = create_minibatch_text_reader(os.path.join(args.datapath, 'ifn_MNIST_Train.txt'), True, total_number_of_samples=args.maxepochs * args.epochsize)
	reader_dev	= create_minibatch_text_reader(os.path.join(args.datapath, 'ifn_MNIST_Development.txt'), True, total_number_of_samples=FULL_DATA_SWEEP)
	reader_test	 = create_minibatch_text_reader(os.path.join(args.datapath, 'ifn_MNIST_Test.txt'), True, total_number_of_samples=FULL_DATA_SWEEP)
	
	# Train the model (with cross validation) (4)
	train_with_cv(reader_train, reader_dev, reader_test , model, max_epochs=args.maxepochs)
	
	# Create a pdf Report (5)
	labreport.evaluate_and_save_pdf(args)

# (1) Create NETWORK with CRITERION
def create_network_and_criterion():

	# (1.1) Input variables denoting the features and label data
	feature_var = cntk.input_variable((num_channels, image_height, image_width))
	label_var = cntk.input_variable((num_classes))

	# (1.2) Apply model to input
	scaled_input = feature_var/256
	z = create_network_structure(num_classes)(scaled_input)

	# (1.3) Define loss and metric
	ce = cross_entropy_with_softmax(z, label_var)
	errs = classification_error(z, label_var)

	return {
		'feature': feature_var,
		'label': label_var,
		'ce' : ce,
		'errs' : errs,
		'output': z
	}

# (2) Create the NETWORK STRUCTURE (CNN/FFN)
def create_network_structure(num_classes):
	with default_options(activation=relu, pad=False, init=glorot_uniform(seed=1)):
		if args.nettype == "ffn":
			return Sequential([
				For(range(num_hidden_layers), lambda : [
					Dense(hidden_layers_dim)
				]),
				Dense(num_classes, activation = None)
			])
		else:
			return Sequential([
				Convolution2D((5,5), 32, pad=True),
				MaxPooling((3,3), (2,2)),
				Convolution2D((3,3), 48),
				MaxPooling((3,3), (2,2)),
				Convolution2D((3,3), 64),
				Dense(96),
				Dropout(args.dropout),
				Dense(num_classes, activation=None)
			])

# (3) Create a TEXT READER (minibatch source)
def create_minibatch_text_reader(path, is_training,	 total_number_of_samples):
	return MinibatchSource(CTFDeserializer(path, StreamDefs(
		features  = StreamDef(field='features', shape=input_dim),
		labels	  = StreamDef(field='labels',	shape=num_classes)
	)), randomize=is_training,	max_samples=total_number_of_samples)

# (4) TRAINING (with cross validation)
def train_with_cv(reader_train, reader_dev, reader_test, model, max_epochs = args.maxepochs, epoch_size = args.epochsize, minibatch_size = args.minibatch):
	train_map = {
		model['feature']: reader_train.streams.features,
		model['label']: reader_train.streams.labels
	}

	# (4.1) Define the progess printer
	progress_printer = ProgressPrinter(tag = 'Training', num_epochs = max_epochs, log_to_file = os.path.join(args.output, args.logfile))

	# (4.2) Define the learner
	learner = momentum_sgd(model['output'].parameters, 
						   lr = learning_rate_schedule(lerningrate, unit = UnitType.sample, epoch_size = epoch_size),
						   momentum = momentum_as_time_constant_schedule(momentum, epoch_size = epoch_size),
						   l2_regularization_weight = args.l2weight)
						   
	# (4.3) Define the trainer
	trainer = Trainer(model['output'], (model['ce'], model['errs']), learner, progress_printer)

	# (4.4) Start the trainign session with the trainerand reader
	if args.nettype == "ffn":
		str_temp = "FeedForwardNet_MNIST_"
	else:
		str_temp = "ConvNet_MNIST_"
	
	training_session(
		trainer = trainer, mb_source = reader_train,
		model_inputs_to_streams = train_map,
		mb_size = minibatch_size,
		progress_frequency = epoch_size,
		checkpoint_config = CheckpointConfig(frequency = epoch_size,
											 preserve_all = True,
											 filename = os.path.join(args.output, str_temp)),
		test_config = TestConfig(reader_test, minibatch_size = minibatch_size),									# Define the test configuration
		cv_config = CrossValidationConfig(reader_dev, minibatch_size = minibatch_size, frequency = epoch_size)	# Define the CV configuration
		).train()

# Run the main function
if __name__=='__main__':
	main()