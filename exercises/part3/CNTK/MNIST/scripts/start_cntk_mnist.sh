#!/bin/bash
#SBATCH --job-name=MNIST
#SBATCH --gres=gpu:GTX1080Ti:1
#SBATCH --partition=dll
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=2gb
#SBATCH --time=01:00:00

# Necessary call, don't change
source /etc/profile >> /dev/null

# Parse commandline arguments
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
# Training specific variables
outputFileName="last_logfile.txt"
scriptName="cntk_mnist.py"
dropout=0.0
dataaug=0
maxepoch=5
minibatchSize=32
l2=0.000

function show_help() { 
	echo "$(basename "$0") [-h -t -e -d -b -l -a] -- program to submit training job to the queue

where:
	-h  show this help text
	-t  set type (FFN, CNN)
	-e  set max num of epochs (default ${maxepoch})
	-d  dropout value p (default ${dropout})
	-b  minibatch size (default ${minibatchSize})
	-l  l2 regularization (default ${l2})
	-a  data augmentation (0=false, 1=true, default ${dataaug})"
}

tFlag=false

while getopts "h?t:e:d:b:l:a:" opt; do
    case "$opt" in
	h|\?)
		show_help
		exit 0
		;;
    t) 
		tFlag=true
		if [[ ${OPTARG} == 'CNN' ]]; then
			nettype="cnn"
		else
			nettype="ffn"
		fi
        ;;
	d)
		dropout=$OPTARG
		;;
	e)
		maxepoch=$OPTARG
		;;
	b)
		minibatchSize=$OPTARG
		;;
	l)
		l2=$OPTARG
		;;
	a)
		dataaug=$OPTARG
		;;
    esac
done

shift $((OPTIND-1))

if ! $tFlag; then
    echo "-t must be specified (FFN, CNN)"
    exit 1
fi

[ "$1" = "--" ] && shift


# All your scripts that are to be executed and the data needs to be in /opt/cluster/data/$username since this folder is shared across nodes
# Head Node dirs of data and scripts - must be in /opt/cluster/data/$username
username=$(whoami)

# absolute path to base directory $pwd gives you the pwd of the directory from which you are calling the script and not necessarily the pwd of the location of the script
dirBase="$(pwd)/scripts/" # assumes we execute the script from one level above the scripts folder since it is moved to the cluster and the slurm output written to the directory the script is executed from

# absolute path to your scripts directory
dirScripts="${dirBase}../scripts/"
tmpDirScripts=${dirScripts:0:-1} # remove trailing slash
dirNameScripts=${tmpDirScripts##*/}

# absolute path to your data directory
dirData="/home/dlldata/part3/MNIST/"
tmpDirData=${dirData:0:-1} # remove trailing slash
dirNameData=${tmpDirData##*/} # this is passed as database argument to the python script

# absolute path to your output directory
dirOutput="${dirBase}../results/${SLURM_JOB_ID}/"

mkdir -p ${dirOutput}

echo "Computing job "${SLURM_JOB_ID}" on "$(hostname)

# load modules and environment
module load cntk/2.3
module load anaconda/3-5.0.1
source activate cntk-23-GPU-py34

# Execute le Job!
execArgs="-mb ${minibatchSize} -me ${maxepoch} -do ${dropout} -l2 ${l2} -log ${outputFileName} -aug ${dataaug} -data ${dirData} -db ${dirNameData} -out ${dirOutput} -nt ${nettype} -pt 3"
echo ${execArgs} > "${dirOutput}config_results"

srun python ${dirScripts}${scriptName} ${execArgs}
