#!/bin/bash
#SBATCH --time=2:00:00
#SBATCH --gres=gpu:1,gpu_mem:2G
#SBATCH --job-name=CNTK_train
#SBATCH --exclude=sbv9_0,sbv9_1,sbv9_2,sbv9_3

source /etc/profile >> /dev/null

#Training specific variables
dnnName=desserts_epoch49

# Head Node dirs of data and scripts
data_head_dir=/opt/cluster/data/$(whoami)/data/
train_path=$(pwd)
datafolder=Char_LSTM
 
# Dir of finished model / results 
 modelDir=models/ 

# Name of training scripts dir
train_scripts_dir=${PWD##*/}
job_scripts_dir=$SLURM_JOB_ID"_"$train_scripts_dir

# Directories on ComputeNode
Cnode_dir=$(whoami)/
Cnode_data_dir=$Cnode_dir"data/"

echo "Computing job "$SLURM_JOB_ID" on "$(hostname)
cd ../../../..
cd run/

# Copy training data to from head node to this_node
if [ ! -d "$Cnode_data_dir/$datafolder" ]; then
  # Control will enter here if $DIRECTORY exists.
  mkdir -p $Cnode_data_dir
  cp -r $data_head_dir$datafolder $Cnode_data_dir
fi


# Copy training scripts for this specific job
if [ ! -d "./run/$Cnode_dir/$job_scripts_dir" ]; then
  # Control will enter here if $DIRECTORY exists.
  mkdir -p $Cnode_dir"tmp"$SLURM_JOB_ID
  cp -r $train_path $Cnode_dir"tmp"$SLURM_JOB_ID
  mv $Cnode_dir"tmp"$SLURM_JOB_ID/$train_scripts_dir $Cnode_dir$job_scripts_dir
  rm -rf $Cnode_dir"tmp"$SLURM_JOB_ID
fi


cd $(whoami)/$job_scripts_dir

if [ ! -d $modelDir ]; then
	mkdir $modelDir
fi

#Execute le Job!
srun -l python char_lstm.py


cp -r $modelDir$dnnName.dnn $train_path"/"$modelDir
cd /opt/cluster/run

rm -rf $Cnode_dir/$job_scripts_dir
#mv /home/$(whoami)/slurm*.out /opt/cluster/data/$(whoami)/$train_dir/
