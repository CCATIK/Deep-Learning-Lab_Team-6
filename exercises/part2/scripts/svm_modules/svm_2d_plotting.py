import numpy as np
import matplotlib.pyplot as plt

##  Adjusts window for data
#   datapoints: Data for which the window should be adjusted (ndarray)
#   dimension: Defines for how many dimensions the window is adjusted (int, 2 ... 3)
#   Returns: ndarray with (X-Min, Y-Min, X-Max, Y-Max)

def adjustWindow(datapoints, dimension, border_margin):
    max_dist = 0
    xy_ax_min = np.amin(datapoints, axis=0)  # Minimum (X-Achse, Y-Achse)
    xy_ax_max = np.amax(datapoints, axis=0)  # Maximum (X-Achse, Y-Achse)
    for i in range(0, datapoints.shape[1] + 2):
        if i < 2:
            if max_dist < np.absolute(xy_ax_min[i]):
                max_dist = np.absolute(xy_ax_min[i])
        else:
            if max_dist < np.absolute(xy_ax_max[i - 2]):
                max_dist = np.absolute(xy_ax_max[i - 2])
    for i in range(0, datapoints.shape[1]):
        xy_ax_min[i] = xy_ax_min[i] - max_dist * border_margin
    for i in range(0, datapoints.shape[1]):
        xy_ax_max[i] = xy_ax_max[i] + max_dist * border_margin
    if dimension == 2:
        return np.array([xy_ax_min[0], xy_ax_min[1], xy_ax_max[0], xy_ax_max[1]])
    elif dimension == 3:
        return np.array([xy_ax_min[0], xy_ax_min[1], xy_ax_min[2], xy_ax_max[0], xy_ax_max[1], xy_ax_max[2]])
    else:
        exit('Error:\tDimension of data must be 2 or 3!')

# function to summarize function calls for plotting
def pltWindow(dataX, dataY, minX, maxX, minY, maxY, rad, zord, n_classes, cmapC):
    reg = np.zeros(n_classes)
    ys = [i + dataX + (i * dataX) ** 2 for i in range(n_classes)]
    colors = cmapC(np.linspace(0, 1, len(ys)))
    for xp, c, m in zip(dataX, dataY, markMap(dataY)):
        if reg[c.astype(int)] == 0:
            plt.scatter(xp[0], xp[1], s=rad, c=colors[c.astype(int)], marker=m, facecolors='none', zorder=zord,
                        edgecolors='black', label = 'Class ' + '{0:g}'.format(c.astype(int)))
            reg[c.astype(int)] = 1
        else:
            plt.scatter(xp[0], xp[1], s=rad, c=colors[c.astype(int)], marker=m, facecolors='none', zorder=zord,
                        edgecolors='black')
    plt.axis([minX, maxX, minY, maxY])    # [low_x, high_x, low_y, high_y]
    plt.xlabel('X-Dimension')
    plt.ylabel('Y-Dimension')

def markMap(label):
    marker_map = []
    for i in range(0, label.size):
        if label[i] == 0:
            marker_map.append('o')
        elif label[i] == 1:
            marker_map.append('X')
        elif label[i] == 2:
            marker_map.append("^")
        elif label[i] == 3:
            marker_map.append("v")
        elif label[i] == 4:
            marker_map.append("P")
        elif label[i] == 5:
            marker_map.append("D")
        elif label[i] == 6:
            marker_map.append("<")
        elif label[i] == 7:
            marker_map.append(">")
        elif label[i] == 8:
            marker_map.append("*")
        elif label[i] == 9:
            marker_map.append("D")
        else:
            marker_map.append(".")
    return np.asarray(marker_map)