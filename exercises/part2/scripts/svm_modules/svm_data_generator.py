import numpy as np
import random
from sklearn.datasets.samples_generator import make_swiss_roll

#   Generates a gaussian classification problem with variable parameters
#   means:          Array, consists out of mean vectors with same dimension, e.g. [[2,4,3],[2,4,2]] for two 3-dimensionals
#   covs:           Array, consits out of corresponding covariance matrices, e.g. [[[1,0,-2],[2,1,0],[3,2,1]],[[1,0,-2],[2,1,0],[3,2,1]]]
#                   for two 3-dimensionals
#   amount_samples: Amount of samples, which are going to be generated
def genGauss(means, covs, pre_label, amount_samples):
    X = []
    Y = []
    if ((len(means.shape) <= 1) or (len(covs.shape) <= 1)):
        exit("Dimension of Data (means or cov-matrices) not equal.\nPlease ensure that dimensions of mean and cov-matrices fit!")
    if pre_label.size == 0 and amount_samples.size == 1:
        if not (means.shape[0] == covs.shape[0]):
            exit(
                "Number of means do not fit to the number of cov-matrices!\nPlease ensure that the amounts are equal.")
    elif pre_label.size == 0:
        if not (means.shape[0] == covs.shape[0] == amount_samples.shape[0]):
            exit(
                "Number of means do not fit to the number of cov-matrices or labels!\nPlease ensure that the amounts are equal.")
    elif amount_samples.size == 1:
        if not (means.shape[0] == covs.shape[0] == pre_label.shape[0]):
            exit(
                "Number of means do not fit to the number of cov-matrices or amount of samples!\nPlease ensure that the amounts are equal.")
    else:
        if not (means.shape[0] == covs.shape[0] == pre_label.shape[0] == amount_samples.shape[0]):
            exit("Number of means do not fit to the number of cov-matrices or labels or amount of samples!\nPlease ensure that the amounts are equal.")
    for i in range(0, means.shape[0]):
        if amount_samples.size == 1:
            gauss_vals = np.random.multivariate_normal(means[i], covs[i], amount_samples[0])
        elif amount_samples.size < 1:
            exit(
                "Amount of samples < 1!\nPlease ensure that the amount is at least 1.")
        else:
            gauss_vals = np.random.multivariate_normal(means[i], covs[i], amount_samples[i])
        labels = np.empty([gauss_vals.shape[0]])
        if pre_label.size == 0:
            labels.fill(i)
        else:
            labels.fill(pre_label[i])
        for k in range(0, gauss_vals.shape[0]):
            X.append(gauss_vals[k])
            Y.append(labels[k])
    combined = list(zip(X, Y))
    random.shuffle(combined)
    X[:], Y[:] = zip(*combined)
    return np.asarray(X), np.asarray(Y)

def genSwiss(amount_samples, roll_noise, seed, roll_factors):
    data = []
    labels = []
    np_ind = [0,2]
    for i in range(0, amount_samples.shape[0]):
        if i == 0:
            X, _ = make_swiss_roll(amount_samples[i], roll_noise[i], seed[i])
        else:
            X, _ = make_swiss_roll(amount_samples[i], roll_noise[i], seed[i])
            X = X / roll_factors[i - 1]
        for u in range(0, len(X)):
            data.append(np.take(X[u], np_ind))
        Y = np.empty([amount_samples[i]])
        Y.fill(i)
        for z in range(0, Y.size):
            labels.append(Y[z])
        combined = list(zip(data, labels))
        random.shuffle(combined)
        data[:], labels[:] = zip(*combined)
    return np.asarray(data), np.asarray(labels)