import numpy as np
from sklearn import datasets
from svm_modules.tu_colormaps import *

# load a data set from file
# file must be formatted in libsvm format
def loadDatasets(file):
    A, B = datasets.load_svmlight_file(file)
    X = A.toarray()
    Y = B.astype(int)
    return X, Y

# Method ensures, that class labels are not random (e.g. -265, -3, 5), instead they get mapped to (0, 1, 2), while the
# mapping which has been applied is displayed.
def mapLabels(dataY, display_mapping):
    unique = np.unique(dataY)               # Get unique labels (0, 0, 0, 1, 1) -> (0, 1)
    a_map = np.argsort(unique)          # Apply sort, returns only indices (lookup numpy doc)
    a = np.sort(unique)                 # Apply sort
    if (a[0] < 0):                      # Shift the set so that the first element is zero, others positive
        a = np.add(a, np.abs(a[0]))
    else:
        a = np.subtract(a, a[0])
    for i, k in zip(range(0, a.size - 1), range(1, a.size)):    # Iterate through set and close gaps (e.g. 1, 4) -> (1, 2)
        if (a[k] != a[i] + 1):
            diff = a[k] - a[i] - 1
            for j in range(k, a.size):
                a[j] = a[j] - diff
    for f in range(0, dataY.size):                                  # Apply mapping to labels
        dataY[f] = a[np.where(a_map == (np.where(unique == dataY[f])[0]))][0]
    if (display_mapping == True):                               # display mapping
        print("************** NEW CLASS MAPPING **************\n") #TODO: warum ist das NEW und warum bedarf es einer Ausgabe für die Studis?
        for u in range(0, a.size):
            if (u != a.size):
                print("Class {old_class} -> Class {new_class}\n".format(old_class=unique[u],
                                                                        new_class=a[np.where(a_map == u)][0]))
            else:
                print("Class {old_class} -> Class {new_class}".format(old_class=unique[u],
                                                                      new_class=a[np.where(a_map == u)][0]))
    return dataY

# apply colormap on data points, depending on their class labels
def apl_colormap(class_labels):
    label_color = []
    for i in range(0, class_labels.size):
        if class_labels[i] == 0:
            label_color.append(cmap[0])
        elif class_labels[i] == 1:
            label_color.append(cmap[1])
        elif class_labels[i] == 2:
            label_color.append(cmap[2])
        elif class_labels[i] == 3:
            label_color.append(cmap[3])
        elif class_labels[i] == 4:
            label_color.append(cmap[4])
        elif class_labels[i] == 5:
            label_color.append(cmap[5])
        elif class_labels[i] == 6:
            label_color.append(cmap[6])
        elif class_labels[i] == 7:
            label_color.append(cmap[7])
        elif class_labels[i] == 8:
            label_color.append(cmap[8])
        else:
            label_color.append(cmap["default"])
    return np.asarray(label_color)

# Splits train set into train and dev with proportion parameter
# Returns: images_train, labels_train, images_dev, labels_dev [ndarray]
# Proportion Parameter: (0.0 [ALL DEV] ... 1.0 [ALL TRAIN]) (float),
def split_tr_dev(images, labels, proportion_train):
    splitter = np.ceil(labels.size * proportion_train).astype(int)
    return images[0:splitter], labels[0:splitter], images[splitter:labels.size], labels[splitter:labels.size]