import numpy as np

### Define new classes

# dataPoint: 3D visuals
# dp_discret_co:        data point fixed position for class search in gird
# dp_co:                data point variable, switching between 27 data points around the fixes position point in the middle
# dp_vis_coordinates:   data point for dicision boundary, placed in the middle of an edge

class dataPoint:
    def __init__(self, dp_discret_co, dp_co, dp_vis_coordinates):
        self.dp_discret_co = dp_discret_co
        self.dp_co = dp_co
        self.dp_vis_coordinates = dp_vis_coordinates

# Normalization: puts point for visuals in the mid of the edge, which was created by two vectors to the data points
def halvingFactor(dim_grid, j, i, k, grid_size, data_point):
    return (dim_grid[0:grid_size][data_point[1]][0:grid_size][data_point[0]][0:grid_size][data_point[2]]) + \
           (((dim_grid[0:grid_size][j][0:grid_size][i][0:grid_size][k]) - (dim_grid[0:grid_size][data_point[1]][0:grid_size][data_point[0]][0:grid_size][data_point[2]])) / 2)

# Gets predicted grid from SVM and searches for edges, which connects two different class predictions
# and spawns points for decision border (sampling)
# data_point: coordinates from central points
def getGridPointDiffs(grid, data_point, grid_size, xD, yD, zD, f_Mode):
    difs = []

    # Implement fast mode, reduce calculation complexity

    if (f_Mode == 1):
        yvars = data_point[1]
        zvars = data_point[2]
    else:
        yvars = data_point[1] - 1
        zvars = data_point[2] - 1
    for i in range(data_point[0] - 1, data_point[0] + 1): # x
        for j in range(yvars, data_point[1] + 1): # y
            for k in range(zvars, data_point[2] + 1): # z
                if not (i < 0 or i > grid_size or j < 0 or j > grid_size or k < 0 or k > grid_size):
                    if (grid[0:grid_size] [j][0:grid_size] [i][0:grid_size] [k]) != (grid[0:grid_size] [data_point[1]][0:grid_size] [data_point[0]][0:grid_size] [data_point[2]]):
                        # add half distance to data point for better generalisation
                        x_half = halvingFactor(xD, j, i, k, grid_size, data_point)
                        y_half = halvingFactor(yD, j, i, k, grid_size, data_point)
                        z_half = halvingFactor(zD, j, i, k, grid_size, data_point)
                        # List Items format: <data_point_discret coordinates [x,y,z]> <distant_point_coordinates [x,y,z]> <coordinates of the new data point for visualisation [x,y,z]>
                        difs.append(dataPoint(np.array([xD[0:grid_size][j][0:grid_size][i][0:grid_size][k],
                                                        yD[0:grid_size][j][0:grid_size][i][0:grid_size][k],
                                                        zD[0:grid_size][j][0:grid_size][i][0:grid_size][k]]),
                                              np.array([xD[0:grid_size][data_point[1]][0:grid_size][data_point[0]][0:grid_size][data_point[2]],
                                                        yD[0:grid_size][data_point[1]][0:grid_size][data_point[0]][0:grid_size][data_point[2]],
                                                        zD[0:grid_size][data_point[1]][0:grid_size][data_point[0]][0:grid_size][data_point[2]]]),
                                              np.array([x_half, y_half, z_half])))
    return difs

# compares two lists, searches for doubled items
def checkForDoubles(lagacyList, appendList):
    for i in range(0, len(appendList)):
        for k in range(0, len(lagacyList)):
            if not ((np.array_equal(appendList[i].dp_discret_co, lagacyList[k].dp_discret_co)) & (np.array_equal(appendList[i].dp_co, lagacyList[k].dp_co)) or
                    (np.array_equal(appendList[i].dp_discret_co, lagacyList[k].dp_co)) & (np.array_equal(appendList[i].dp_co, lagacyList[k].dp_discret_co))):
                        lagacyList.append(appendList[i])
                        break
    return lagacyList

# iterates through predicted class grid and calculates for every point the the edges, which are related to a change of class (for example one adjecent point
# goes to class 0, the other adjacent to class 1, then the edge is detected and highlighted as decision border supportive
'''      (decision border)
   x(0)--\       |       /--x(1)
          ---\   |   /---
   x(0)----------|--(edge)--x(1)                -- x(CLASS) --
          ---/   |   \---     
   x(0)--/       |       \--x(1)
'''

def getFunctPlane(pred_gred, grid_size, xD, xY, xZ, f_Mode):
    functPlanePoints = []
    for i in range(0, grid_size): # x
        for j in range(0, grid_size): # y
            for k in range(0, grid_size): # z
                dif_list = getGridPointDiffs(pred_gred, np.array([i, j, k]), grid_size, xD, xY, xZ, f_Mode)
                if not functPlanePoints:
                    functPlanePoints = functPlanePoints + dif_list
                else:
                    functPlanePoints = checkForDoubles(functPlanePoints, dif_list)
    sample = []
    for i in range(0, len(functPlanePoints)):
        sample.append(functPlanePoints[i].dp_vis_coordinates)
    return np.asarray(sample)