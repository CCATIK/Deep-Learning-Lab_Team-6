#!/usr/bin/env python

# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- (1) HEADERS --------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

# This script generates and imports datasets for various dimensions, trains Support Vector Machines with previously set
# parameters. Results are displayed in dimension d = 3 and outputs the classification accuracy gained with the trained model

__author__ = "Markus Kamp"
__copyright__ = "Copyright 2017, Institute for Communications Technology (IfN), TU Braunschweig"
__version__ = "1.1.2"
__maintainer__ = "Markus Kamp"
__email__ = "m.kamp@tu-bs.de"
__status__ = "Pre-Release"

# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- (2) IMPORTS --------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #


# 2D Imports for scikit
from sklearn import svm

# 3D Imports for PYQT
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl

# Random data generators
from svm_modules.svm_data_generator import *

# Utilities
from svm_modules.svm_util import *

# 2D-Plotting (Here due to adjustWindow function)
from svm_modules.svm_2d_plotting import *

# 3D-Plotting
from svm_modules.svm_3d_border_gen import *

# -------------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- (3) SVM PARAMETERS -------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

## General
predefined_train =          False   # Use predefined dataset for training (bool, default: False)
use_test_data =             False   # Generate data for test and calc accuracy on this set too (bool, default: False)
predefined_test =           False   # Use predefined dataset for test (bool, default: False)
seed_gen =                  110     # Seed for random data generation (training and test, all classes)
                                    # 0: No seed used (random), > 0: Seed used (int, default: 0)
resolution_3d_grid_plot =   100     # Resolution for scatter/sampling grid used in 3D plotting. Values over 200 could
                                    # result in very poor performance! (int, default: 100)
fast_mode_3d =              True    # Reduce complexity while calculating decision boundary in 3D. Faster visualization,
                                    # but resolution of the mesh will decrease! (bool, default: False)
plt_apl_color =             True    # Ink 3D scatter plot (color shows the associated boundaries, which is divided by
                                    # the colored border.) (bool, default: True)
plot_border_margin =        0.1     # Margin between plots edge and nearest data vector in 3D (float, default: 0.1)
cache =                     4000    # Cache size (in MB) for SVM training, adjust to your system memory size
                                    # (float, default: 4000)

# ----------------------------------------- TRAINING PARAMETERS ------------------------------------------------------ #

### Training

## Random data generation

# Random gaussian data generation

if seed_gen > 0:
    np.random.seed(seed_gen)

amount_samples_train = np.array([100])             # Amount samples for training (Generator)

# Array, consists out of mean vectors with same dimension, e.g. [[2,4,3],[2,4,2]]
# for two 3-dimensionals
means_train = np.array([[-2, 2, 2],
                        [2, -2, 2],
                        [6, -6, 2]])   # Array, consists out of mean vectors with same dimension, e.g. [[2,4,3],[2,4,2]] for two 3-dimensionals

#   Array, consists out of corresponding covariance matrices, e.g. [[[1,0,-2],[2,1,0],[3,2,1]],[[1,0,-2],[2,1,0],[3,2,1]]]
#   for two 3-dimensionals

covs_train = np.array([[[1,0,0],
                        [0,1,0],
                        [0,0,1]],

                       [[1,0,0],
                        [0,1,0],
                        [0,0,1]],
                       [[1, 0, 0],
                        [0, 1, 0],
                        [0, 0, 1]]])

# Array, consists out of labels for data clusters, e.g. [0,1] for two clusters with labels for class 0 and 1
# If labels shall be generated automatically, just let array empty! -> np.array([])
labels_train = np.array([0, 1, 0])

## Predefined data train

predefine_file_train = "..\\data\\a.dat"            # File with predefined data

### SVM-Training

penalty =                   1.0         # Penalty parameter C of the error term (float, default: 1.0)
kernel_function =           'sigmoid'    # Specifies the kernel type to be used in the algorithm
                                        # It must be one of 'linear', 'poly', 'rbf', 'sigmoid'
                                        # (string, default: 'rbf')
poly_degree =               3           # Degree of the polynomial kernel function ('poly')
                                        # Ignored by all other kernels
                                        # (int, default: 3)
kernel_coeff =              'auto'      # Kernel coefficient for 'rbf', 'poly' and 'sigmoid'
                                        # If gamma is 'auto' then 1/n_features will be used instead
                                        # (float, default: 'auto'
zero_coeff =                2.0         # Independent term in kernel function
                                        # It is only significant in 'poly' and 'sigmoid'
                                        # (float, default: 0.0)
stopping_criterion =        1e-3        # Tolerance for stopping criterion (float, default: 1e-3)
max_iterations =            -1          # Hard limit on iterations within solver, or -1 for no limit (int, default: -1)

# ----------------------------------------- TEST PARAMETERS ---------------------------------------------------------- #

### Test

## Random data generation

# Random gaussian data generation

amount_samples_test = np.array([100])               # Amount samples for test (Generator)
means_test = np.array([[-2,
                         2,
                         2],

                        [2,
                         -2,
                         2]])            # Array, consists out of mean vectors with same dimension, e.g. [[2,4,3],[2,4,2]] for two 3-dimensionals

#   Array, consits out of corresponding covariance matrices, e.g. [[[1,0,-2],[2,1,0],[3,2,1]],[[1,0,-2],[2,1,0],[3,2,1]]]
#   for two 3-dimensionals

covs_test = np.array([[[1,0,0],
                       [0,1,0],
                       [0,0,1]],

                      [[1,0,0],
                       [0,1,0],
                       [0,0,1]]])

# Array, consists out of labels for data clusters, e.g. [0,1] for two clusters with labels for class 0 and 1
# If labels shall be generated automatically, just let array empty! -> np.array([])

labels_test = np.array([0,1])

## Predefined data test

predefine_file_test = 'data_a1.test'    # File with predefined data

### Define colormap

# -------------------------------------------------------------------------------------------------------------------- #
# ----------------------------------------- (4) FUNCTION DEFINITIONS ------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

# start qt app window as long as user does not quit
def startQTApp():
    ## Start Qt event loop unless running in interactive mode.
    if __name__ == '__main__':
        import sys

        if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
            QtGui.QApplication.instance().exec_()
    return 0

# -------------------------------------------------------------------------------------------------------------------- #
# ----------------------------------------- (5) CREATE/IMPORT DATASETS ----------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

### Generate a random n-class classification problem.

if predefined_train == False:
    print("Generate Training Data Set: {sample} samples\n".format(sample=amount_samples_train))
    X, Y = genGauss(means_train, covs_train, labels_train, amount_samples_train)
    number_classes = len(set(Y))
    dimension_data = X.shape[1]
else:
    print("Import data for training\n")
    #TODO: print-Ausgabe: wieviele samples wurden geladen?
    X, Y = loadDatasets(predefine_file_train)
    number_classes = len(set(Y))
    dimension_data = X.shape[1]

#TODO: Hier ein Kommentar: Was genau ist Y. Vektor mit Labels?
Y = mapLabels(Y, True)

# Check for invalid inputs
if (predefined_test == True or predefined_train == True):
    print("You are using extern data import, pay attention that the dimension_data parameter fits to the data\n")
    print("which you want to import! Otherwise error will occur!\n")

if (number_classes > 9) | (number_classes < 2):
    exit('Error:\tAmount of data classes: 1 < number_classes < 10!\n\t\tAdjust value in the properties section!\n')

if (dimension_data < 2) | (dimension_data > 3):
    print("You have set dimension to a non-representable value. There will only be text output!\n")

# Test data
if use_test_data == True:
    if predefined_test == False:
        print("Generate Test Data Set: {sample} samples\n".format(sample=amount_samples_test))
        XT, YT = genGauss(means_test, covs_test, labels_test, amount_samples_test)
    else:
        print("Import data for test\n")
        XT, YT = loadDatasets(predefine_file_test)
    YT = mapLabels(YT, False)

# -------------------------------------------------------------------------------------------------------------------- #
# ----------------------------------------- (6) TRAINING OF SVM ------------------------------------------------------ #
# -------------------------------------------------------------------------------------------------------------------- #

if dimension_data == 3:

    ## 3D Visuals
    train_adj = adjustWindow(X, 3, plot_border_margin)

    xx, yy, zz = np.meshgrid(np.linspace(train_adj[0], train_adj[3], resolution_3d_grid_plot),
                             np.linspace(train_adj[1], train_adj[4], resolution_3d_grid_plot),
                             np.linspace(train_adj[2], train_adj[5], resolution_3d_grid_plot))

    # fit the model
    clf = svm.SVC(C=penalty, kernel=kernel_function, degree=poly_degree, gamma=kernel_coeff, coef0=zero_coeff,
                  tol=stopping_criterion, max_iter=max_iterations, decision_function_shape='ovr', cache_size=cache)
    print("Train Support Vector Machine model.......")
    clf.fit(X, Y)
    print("finished!\n")


    if (fast_mode_3d == True):
        print("Calculating 3D decision boundry in rapid mode: loss of resolution will occur!\n")

    # Calculate the distance from the separating hyperplane of the SVM for the
    # whole space using the grid defined in the beginning
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel(), zz.ravel()])

    # Generate surface for train data points
    app = QtGui.QApplication([])
    dpTr = gl.GLViewWidget()
    dpTr.opts['distance'] = 40
    dpTr.show()
    acc_str_train = ('Data Points Training + Result SVM-Training - Accuracy on training data = ' + '{0:g}%'.format(clf.score(X, Y) * 100) + "\n")
    dpTr.setWindowTitle(acc_str_train)

    dpTrain = gl.GLScatterPlotItem(pos=X, size=0.3, color=apl_colormap(Y), pxMode=False)
    dpTr.addItem(dpTrain)

    axis3DSizePos = gl.GLAxisItem()
    axis3DSizeNeg = gl.GLAxisItem()
    axis3DSizePos.setSize(train_adj[3], train_adj[4], train_adj[5])
    axis3DSizeNeg.setSize(train_adj[0], train_adj[1], train_adj[2])
    dpTr.addItem(axis3DSizePos)
    dpTr.addItem(axis3DSizeNeg)

    # Generate surface for test data points
    if use_test_data == True:
        test_adj = adjustWindow(XT, 3, plot_border_margin)
        dpTe = gl.GLViewWidget()
        dpTe.opts['distance'] = 40
        dpTe.show()
        acc_str_test = ('Data Points Test + Result SVM-Train - Accuracy on test data = ' + '{0:g}%'.format(clf.score(XT, YT) * 100) + "\n")
        dpTe.setWindowTitle(acc_str_test)

        dpTest = gl.GLScatterPlotItem(pos=XT, size=0.3, color=apl_colormap(YT), pxMode=False)

        axis3DSizePos = gl.GLAxisItem()
        axis3DSizeNeg = gl.GLAxisItem()
        axis3DSizePos.setSize(test_adj[3], test_adj[4], test_adj[5])
        axis3DSizeNeg.setSize(test_adj[0], test_adj[1], test_adj[2])
        dpTe.addItem(axis3DSizePos)
        dpTe.addItem(axis3DSizeNeg)

        dpTe.addItem(dpTest)

    Z = Z.reshape(xx.shape)
    sample_data_points = getFunctPlane(Z, resolution_3d_grid_plot, xx, yy, zz, fast_mode_3d)
    if (plt_apl_color == True):
        clslabels = clf.predict(sample_data_points)
        sp2 = gl.GLScatterPlotItem(pos=sample_data_points, size=0.05, color=apl_colormap(clslabels), pxMode=False)
    else:
        sp2 = gl.GLScatterPlotItem(pos=sample_data_points, size=0.05, color=cmap["default"], pxMode=False)
    dpTr.addItem(sp2)
    if use_test_data == True:
        dpTe.addItem(sp2)
    startQTApp()
else:
    exit('Error:\tDimension of generated data is not in 2d space!\n\t\tAdjust values in the properties section!\n')

print("Training finished successfully, closing application!")