# CNTK SVHN CNN
# Deep Learning Lab - IfN 2017
# ==============================================================================

# Import libraries
from __future__ import print_function
import re
import os
import sys
import math
import argparse
import numpy as np
import matplotlib

# Extend python search path for utils dir
sys.path.append(os.path.expanduser("~") + "/utils")

import labreport
matplotlib.use('agg')
import matplotlib.pyplot as plt
import cntk
import _cntk_py
import cntk.io.transforms as xforms
from cntk.io import MinibatchSource, ImageDeserializer, StreamDef, StreamDefs, INFINITELY_REPEAT, FULL_DATA_SWEEP
from cntk.layers import Convolution2D, MaxPooling, AveragePooling, Dropout, BatchNormalization, Dense, default_options, identity, Sequential, For

from cntk.train.training_session import *
from cntk.logging import *
from cntk.debugging import *
from cntk.initializer import *

from cntk.layers.typing import *
from cntk import Trainer
from cntk.learners import momentum_sgd, learning_rate_schedule, UnitType, momentum_as_time_constant_schedule
from cntk import cross_entropy_with_softmax, classification_error, relu
from cntk.ops import Function
from cntk.debugging import set_computation_network_trace_level

from cntk.device import try_set_default_device, gpu
try_set_default_device(gpu(0))
from _cntk_py import set_fixed_random_seed, force_deterministic_algorithms
set_fixed_random_seed(1)
force_deterministic_algorithms()

np.random.seed(12345)

# Set variables
parser = argparse.ArgumentParser(description='This script is used to train a CNN for SVHN image recognition. The following  parameters can be assigned:')
parser.add_argument('-mb','--minibatch',help='Minibatch size', required=False, default='64')
parser.add_argument('-do','--dropout',help='Dropout value', required=False, default='0.0')
parser.add_argument('-l2','--l2weight',help='L2 regularization weight', required=False, default='0.002')
parser.add_argument('-lr','--learningrate',help='Learning rate (shedule)', required=False, default='[0.0015625]*20+[0.00046875]*20+[0.00015625]*20+[0.000046875]*10+[0.000015625]')
parser.add_argument('-mo','--momentum',help='Momentum (shedule)', required=False, default='[0]*20+[600]*20+[1200]')
parser.add_argument('-me','--maxepochs',help='Number of training epochs', required=False, default='80')
parser.add_argument('-eps','--epochsize',help='Size of training epoch', required=False, default='73258')
parser.add_argument('-log','--logfile',help='Save trainer output to logfile', required=False, default='logfile.txt')
parser.add_argument('-aug','--dataaug',help='Use data augmentation', required=False, default='False')
parser.add_argument('-data', '--datapath', help='Provide input data path', required=True)
parser.add_argument('-db', '--database', help='Provide database name', required=True)
parser.add_argument('-out', '--output', help='Absolute path to output directory', required=True)
parser.add_argument('-gpu', '--dataongpu', help='Load the complete dataset into the GPU memory', required=False, default='True')
parser.add_argument('-pt', '--part', help='Lab part: 3 or 4', required=True, type=int)
args = parser.parse_args()
 
minibatch = int(args.minibatch)
dropout = float(args.dropout)
l2weight = float(args.l2weight)
maxepochs = int(args.maxepochs)
epochsize = int(args.epochsize)
logfile = args.logfile
dataaug = bool(args.dataaug)
data_path = args.datapath
database = args.database
dataongpu = bool(args.dataongpu)
exec('lerningrate={}'.format(args.learningrate))
exec('momentum={}'.format(args.momentum))

# Set paths (relative to current python file)
database = 'SVHN'
model_path = "models"

# Set model dimensions
image_height = 32
image_width = 32
num_channels = 3  # RGB
num_classes = 10

def main():
    # Create the model (1) and (2)
    model = create_network_and_criterion()    
    
    # Create readers (3)
    reader_train = create_minibatch_image_reader(os.path.join(data_path, 'ifn_SVHN_Training.txt'), os.path.join(data_path, 'SVHN_mean.xml'), use_transform = dataaug, total_number_of_samples = maxepochs * epochsize, use_randomize = True)
    reader_dev  = create_minibatch_image_reader(os.path.join(data_path, 'ifn_SVHN_Development.txt'),  os.path.join(data_path, 'SVHN_mean.xml'), False, total_number_of_samples = FULL_DATA_SWEEP,use_randomize = True)
    
    # Train the model (with cross validation) (4)
    train_with_cv(reader_train, reader_dev , model, max_epochs=maxepochs)
    
    # Create a pdf Report (5)
    labreport.evaluate_and_save_pdf(args) 

# (1) Create NETWORK with CRITERION
def create_network_and_criterion():

     # (1.1) Input variables denoting the features and label data
    feature_var = cntk.input_variable((num_channels, image_height, image_width))
    label_var = cntk.input_variable((num_classes))

    # (1.2) Apply model to input
    scaled_input = feature_var/256
    z = create_network_structure(num_classes)(scaled_input)

    # (1.3) Define loss and metric
    ce = cross_entropy_with_softmax(z, label_var)
    errs = classification_error      (z, label_var)

    return {
        'feature': feature_var,
        'label': label_var,
        'ce' : ce,
        'errs' : errs,
        'output': z
    }    
    
# Create the NETWORK STRUCTURE
def create_network_structure(num_classes):
    with default_options(activation = relu, pad = True, init = glorot_uniform(seed = 1)):
        return Sequential([
            For(range(2), lambda : [
                Convolution2D((3,3), 64),              
                Convolution2D((3,3), 64),   
                MaxPooling((3,3), strides = 2)  
            ]), 
            For(range(2), lambda i: [
                Dense([256,128][i]),               
                Dropout(dropout)
            ]), 
            Dense(num_classes, activation = None) 
        ])

# (3) Create an IMAGE READER (minibatch source)       
def create_minibatch_image_reader(map_file, mean_file, use_transform, total_number_of_samples, use_randomize):
   if not os.path.exists(map_file) or not os.path.exists(mean_file):
       raise RuntimeError("File {} or {} does not exist.".format(map_file, mean_file))

   # (3.1) Transformation pipeline for the features has jitter/crop only when training
   transforms = []
   if use_transform:
       transforms += [
           xforms.crop(crop_type = 'randomside', side_ratio = 0.8, jitter_type = 'uniratio') # train uses jitter
       ]
   transforms += [
       xforms.scale(width = image_width, height = image_height, channels = num_channels, interpolations = 'linear'),
       xforms.mean(mean_file)
   ]
   
   # (3.2) Deserializer
   return MinibatchSource(ImageDeserializer(map_file, StreamDefs(
        features = StreamDef(field = 'image', transforms = transforms), # 1st col in mapfile referred to as 'image'
        labels = StreamDef(field = 'label', shape = num_classes))),        # and second as 'label'
        randomization_seed = 12345,
        randomize = use_randomize,
        max_samples = total_number_of_samples)

		
# (4) TRAINING (with cross validation)
def train_with_cv(reader_train, reader_dev, model, max_epochs = maxepochs, epoch_size = epochsize, minibatch_size = minibatch):
    train_map = {
        model['feature']: reader_train.streams.features,
        model['label']: reader_train.streams.labels
    }
   
    # (4.1) Define the progess printer
    progress_printer = ProgressPrinter(tag = 'Training', num_epochs = max_epochs, log_to_file = os.path.join(args.output, args.logfile))

    # (4.2) Define the learner
    learner = momentum_sgd(model['output'].parameters, 
                           lr = learning_rate_schedule(lerningrate, unit = UnitType.sample, epoch_size = epoch_size),
                           momentum = momentum_as_time_constant_schedule(momentum, epoch_size = epoch_size),
                           l2_regularization_weight = l2weight)
                           
    # (4.3) Define the trainer
    trainer = Trainer(model['output'], (model['ce'], model['errs']), learner, progress_printer)
    
    # (4.4) Start the trainign session with the trainerand reader
    training_session(
        trainer = trainer, mb_source = reader_train,
        model_inputs_to_streams = train_map,
        mb_size = minibatch_size,
        progress_frequency = epoch_size,
        checkpoint_config = CheckpointConfig(frequency = epoch_size,
                                             preserve_all = True,
                                             filename = os.path.join(args.output, "ConvNet_SVHN_")),                                                   # Define the test configuration
        cv_config = CrossValidationConfig(reader_dev, minibatch_size = minibatch_size, frequency = epoch_size)  # Define the CV configuration
        ).train()
     
# Run the main function
if __name__=='__main__':
    main()
