#!/bin/bash
#SBATCH --job-name=svhn.tr
#SBATCH --gres=gpu:GTX1080Ti:1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=8gb
#SBATCH --time=01:00:00

# Necessary call, don't change
source /etc/profile >> /dev/null

# Parse commandline arguments
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Training specific variables
dnnName="CNN_SVHN_"
outputFileName="last_logfile.txt"
dropout=0.0
dataaug=False
maxepoch=5
minibatchSize=32
l2=0.002

function show_help() { 
	echo "$(basename "$0") [-h -t -e -d -b -l -a] -- program to submit training job to the queue

where:
	-h  show this help text
	-t  set type (FFN, CNN)
	-e  set max num of epochs (default ${maxepoch})
	-d  dropout value p (default ${dropout})
	-b  minibatch size (default ${minibatchSize})
	-l  l2 regularization (default ${l2})
	-a  data augmentation (0=false, 1=true, default ${dataaug})"
}

tFlag=false

while getopts "h?t:e:d:b:l:a:" opt; do
    case "$opt" in
	h|\?)
		show_help
		exit 0
		;;
    t) 
		tFlag=true
		if [[ ${OPTARG} == 'CNN' ]]; then
			dnnName="CNN_SVHN_"
			scriptName="cntk_cnn_svhn.py"
		else
			echo "FFN not available for SVHN"
		fi
        ;;
	d)
		dropout=$OPTARG
		;;
	e)
		maxepoch=$OPTARG
		;;
	b)
		minibatchSize=$OPTARG
		;;
	l)
		l2=$OPTARG
		;;
	a)
		dataaug=$OPTARG
		;;
    esac
done

shift $((OPTIND-1))

if ! $tFlag; then
    echo "-t must be specified (FFN, CNN)"
    exit 1
fi

[ "$1" = "--" ] && shift


# absolute path to base directory $pwd gives you the pwd of the directory from which you are calling the script and not necessarily the pwd of the location of the script
dirBase="$(pwd)/scripts/" # assumes we execute the script from one level above the scripts folder since it is moved to the cluster and the slurm output written to the directory the script is executed from

# absolute path to your scripts directory
dirScripts="${dirBase}../scripts/"
tmpDirScripts=${dirScripts:0:-1} # remove trailing slash
dirNameScripts=${tmpDirScripts##*/}

# absolute path to your data directory
dirData="/home/dlldata/part4/SVHN/"
tmpDirData=${dirData:0:-1} # remove trailing slash
dirNameData=${tmpDirData##*/} # this is passed as database argument to the python script

# absolute path to your output directory
dirOutput="${dirBase}../results/${SLURM_JOB_ID}/"

mkdir -p ${dirOutput}

echo "Computing job "${SLURM_JOB_ID}" on "$(hostname)

# load modules and environment
module load lib/openmpi/1.10.3
module load lib/jpeg/8.0
module load lib/png/1.2.59
module load anaconda/3-5.0.1
source activate cntk-231-GPU-py34

# Execute le Job!
execArgs="-mb ${minibatchSize} -me ${maxepoch} -do ${dropout} -l2 ${l2} -log ${outputFileName} -aug ${dataaug} -data ${dirData} -db ${dirNameData} -out ${dirOutput} -pt 4"
echo ${execArgs} > "${dirOutput}config_results"

srun python ${dirScripts}${scriptName} ${execArgs}
