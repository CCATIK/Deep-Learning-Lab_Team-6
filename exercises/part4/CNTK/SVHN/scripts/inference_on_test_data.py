# CNTK SVHN Inference
# Deep Learning Lab - IfN 2018
# ==============================================================================

# Import libraries
from __future__ import print_function
import re
import os
import argparse
import numpy as np
import cntk
import _cntk_py
import cntk.io.transforms as xforms
from cntk.ops.functions import load_model
from cntk.io import MinibatchSource, ImageDeserializer, StreamDef, StreamDefs, INFINITELY_REPEAT, FULL_DATA_SWEEP
from cntk.layers import Convolution2D, MaxPooling, AveragePooling, Dropout, BatchNormalization, Dense, default_options, identity, Sequential, For
from cntk.device import try_set_default_device, gpu
try_set_default_device(gpu(0))
from _cntk_py import set_fixed_random_seed, force_deterministic_algorithms
set_fixed_random_seed(1)
force_deterministic_algorithms()
np.random.seed(12345)

# Set variables
parser = argparse.ArgumentParser(description='This script is used for inference on the SVHN test data. The following  parameters can be assigned:')
parser.add_argument('-id','--experimentid',help='ID of the experiment folder in which the model is saved', required=True)
parser.add_argument('-data', '--datapath', help='Provide input data path', required=True)
parser.add_argument('-out', '--output', help='Absolute path to output directory', required=True)
args = parser.parse_args()
 
experiment_id = args.experimentid
data_path = args.datapath
output_path = args.output

# Set paths (relative to current python file)
model_path = './results/{}/ConvNet_SVHN_'.format(experiment_id)

# Set model dimensions
image_height = 32
image_width = 32
num_channels = 3  # RGB
num_classes = 10

def main():
    # Load model (1)
    model = load_model(model_path)    
        
    # Create test data reader (2)
    reader_test  = create_minibatch_image_reader(os.path.join(data_path, 'ifn_SVHN_Test_nolabel.txt'),  os.path.join(data_path, 'SVHN_mean.xml'))
    
    # Inference on test set (3)
    inference_results = inference_on_test_data(reader_test , model)   

    # Save inference  results to txt file (4)
    save_inference_results(data_path,inference_results)

# (2) Create an IMAGE READER (minibatch source)       
def create_minibatch_image_reader(map_file, mean_file):
    if not os.path.exists(map_file) or not os.path.exists(mean_file):
        raise RuntimeError("File {} or {} does not exist.".format(map_file, mean_file))

    # (2.1) Transformation pipeline for the features has jitter/crop only when training
    transforms = []
    transforms += [xforms.scale(width = image_width, height = image_height, channels = num_channels, interpolations = 'linear'),xforms.mean(mean_file)]
   
   # (2.2) Deserializer
    return MinibatchSource(ImageDeserializer(map_file, StreamDefs(
        features = StreamDef(field = 'image', transforms = transforms))),       
        randomize = False,
        max_sweeps = 1)

# (3) Inference on test data
def inference_on_test_data(reader_test , model):
    out = cntk.softmax(model.outputs[0])
    inference_minibatch_size = 64
    eval_input_map = {'images': reader_test.streams.features}
    
    results = []
    proc_inference = True
    while proc_inference:
        try:
            data = reader_test.next_minibatch(inference_minibatch_size, input_map=eval_input_map)
            img_data = data['images'].asarray()
            predicted_labels = [np.argmax(out.eval(img_data[i], device = cntk.device.gpu(0))) for i in range(len(img_data))]
            results.extend(predicted_labels)
        except KeyError:
            proc_inference = False
    return results

# (4) Inference results to txt file
def save_inference_results(data_path,results):
    with open(os.path.join(data_path, 'ifn_SVHN_Test_nolabel.txt'), 'r') as f:
        file_lines = [''.join([line.strip('0\n\r'), str(value),'\n']) for line, value in zip(f.readlines(), results)]
      
    with open('inference_results/svhn_inference_test.txt', 'w') as f:
        f.writelines(file_lines) 

# Run the main function
if __name__=='__main__':
    main()
