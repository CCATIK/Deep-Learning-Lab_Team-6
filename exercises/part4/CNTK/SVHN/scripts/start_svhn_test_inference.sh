#!/bin/bash
#SBATCH --job-name=svhn.tr
#SBATCH --gres=gpu:GTX1080Ti:1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=8gb
#SBATCH --time=01:00:00

# Necessary call, don't change
source /etc/profile >> /dev/null

scriptName="inference_on_test_data.py"

# Parse commandline arguments
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

function show_help() { 
	echo "$(basename "$0") [-h -i] -- program to submit training job to the queue

where:
	-h  show this help text
	-i  set id of best training"
}

iFlag=false

while getopts "h?i:" opt; do
    case "$opt" in
	h|\?)
		show_help
		exit 0
		;;
	i)
		iFlag=true
		netId=$OPTARG
		;;
    esac
done

shift $((OPTIND-1))

if ! $iFlag; then
    echo "-i must be specified (id of your slurm job)"
    exit 1
fi

[ "$1" = "--" ] && shift

if [ ! -d "results/${netId}/" ]; then
	echo "Invalid slurm jobid, directory results/${netId} not found."
	exit 1
fi

# absolute path to base directory $pwd gives you the pwd of the directory from which you are calling the script and not necessarily the pwd of the location of the script
dirBase="$(pwd)/scripts/" # assumes we execute the script from one level above the scripts folder since it is moved to the cluster and the slurm output written to the directory the script is executed from

# absolute path to your scripts directory
dirScripts="${dirBase}../scripts/"
tmpDirScripts=${dirScripts:0:-1} # remove trailing slash
dirNameScripts=${tmpDirScripts##*/}

# absolute path to your data directory
dirData="/home/dlldata/part4/SVHN/"
tmpDirData=${dirData:0:-1} # remove trailing slash
dirNameData=${tmpDirData##*/} # this is passed as database argument to the python script

# absolute path to your output directory
dirOutput="${dirBase}../inference_results/"

mkdir -p ${dirOutput}

echo "Computing job "${SLURM_JOB_ID}" on "$(hostname)

# load modules and environment
module load lib/openmpi/1.10.3
module load lib/jpeg/8.0
module load lib/png/1.2.59
module load anaconda/3-5.0.1
source activate cntk-231-GPU-py34

# Execute le Job!
execArgs="-id ${netId} -data ${dirData} -out ${dirOutput}"

srun python ${dirScripts}${scriptName} ${execArgs}
